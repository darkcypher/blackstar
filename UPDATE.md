# updating
the database might need migration.

## checking
check media database version:
```shell
sqlite3 /var/blackstar/database/media.sqlite 'PRAGMA user_version;'
```

if it says `1`, manual migration steps are required.

## migration
stop all services first, then:
```shell
cd /var/blackstar/database
mv media.sqlite media_old.sqlite
sqlite3 media.sqlite
```

initialize the database schema:
```
.read /opt/blackstar/blackstar/common/schema/media.sql
```

attach old database:
```sql
ATTACH DATABASE 'media_old.sqlite' AS v1;
```

migrate metadata table:
```sql
INSERT INTO metadata (
	device, inode, type, size,
	atime, mtime, ctime,
	hash_md5, hash_sha1, hash_sha256,
	width, height, duration,
	video_codec, audio_codec,
	exif_make, exif_model, exif_software, exif_latitude, exif_longitude,
	perceptual,
	thumbnail_1x, thumbnail_2x, thumbnail_3x, thumbnail_4x,
	preview_1x, preview_2x,
	count_link, count_similar, count_duplicate
) SELECT
	device, inode, type, size,
	atime, mtime, ctime,
	hash_md5, hash_sha1, hash_sha256,
	width, height, duration,
	vcodec, acodec,
	exif_make, exif_model, exif_software, exif_latitude, exif_longitude,
	perceptual,
	thumbnail_1x, thumbnail_2x, thumbnail_3x, thumbnail_4x,
	preview_1x, preview_2x,
	count_link, count_similar, count_duplicate
FROM v1.metadata;
```

copy other tables:
```sql
INSERT INTO hierarchy SELECT * FROM v1.hierarchy;
INSERT INTO similar SELECT * FROM v1.similar;
INSERT INTO queue SELECT * FROM v1.queue;
```

detach old database:
```sql
DETACH DATABASE v1;
```

enqueue basic task for all affected nodes:
```sql
INSERT OR IGNORE INTO queue (device, inode, task)
	SELECT device, inode, 'basic'
	FROM metadata
	WHERE type = 'I' OR type = 'A' OR type = 'V';
```

exit:
```
.quit
```

then restart services and let it rescan.
