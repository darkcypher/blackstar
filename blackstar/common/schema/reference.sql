PRAGMA journal_mode = WAL;
PRAGMA user_version = 3;

CREATE TABLE reference (
	id TEXT NOT NULL UNIQUE,
	expiration INTEGER,
	token TEXT,
	path PATH BLOB NOT NULL,
	download INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY("id")
);

CREATE UNIQUE INDEX reference_id ON reference (id);
CREATE INDEX reference_path ON reference (path);
CREATE INDEX reference_all ON reference (expiration, token, path, download);
