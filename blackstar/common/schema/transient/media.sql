ATTACH DATABASE ':memory:' AS transient;

CREATE TABLE transient.recursive (
	device_container INTEGER NOT NULL,
	inode_container INTEGER NOT NULL,
	device_item INTEGER NOT NULL,
	inode_item INTEGER NOT NULL
);

CREATE INDEX transient.recursive_container ON recursive (device_container, inode_container);
CREATE UNIQUE INDEX transient.recursive_all ON recursive (device_container, inode_container, device_item, inode_item);
