ATTACH DATABASE ':memory:' AS transient;

CREATE TABLE transient.bucket (
	path PATH BLOB NOT NULL UNIQUE
);

CREATE UNIQUE INDEX transient.bucket_path ON bucket(path);
