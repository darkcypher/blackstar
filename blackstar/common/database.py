import contextlib
import importlib.resources
import os
import pathlib
import sqlite3
import typing
import urllib.request

from .configuration import Configuration, Synchronous
from .logging import LOGGER


def _serialize_path(path: pathlib.PurePosixPath) -> bytes:
	return os.fsencode(os.fspath(path))


def _deserialize_path(data: bytes) -> pathlib.PurePosixPath:
	return pathlib.PurePosixPath(os.fsdecode(data))


sqlite3.register_adapter(pathlib.PurePosixPath, _serialize_path)
sqlite3.register_converter('PATH', _deserialize_path)


def format_uri(path: str, mode: str) -> str:
	return 'file:{}?mode={}'.format(urllib.request.pathname2url(path), mode)


class Connection(sqlite3.Connection):
	def __enter__(self) -> typing.Self:
		self.execute('BEGIN IMMEDIATE')
		return self


def open_schema(schema: str, transient: bool = False) -> typing.IO[str]:
	resource = importlib.resources.files(__package__) / 'schema'
	if transient:
		resource /= 'transient'
	resource /= '{}.sql'.format(schema)
	return resource.open('r', encoding='utf-8')


def configure_database(db: sqlite3.Connection, synchronous: Synchronous) -> None:
	db.execute('PRAGMA synchronous = {}'.format(synchronous.name))
	db.execute('PRAGMA foreign_keys = ON')


def open_database(path: typing.Union[str, os.PathLike[str]], schema: str, writable: bool, synchronous: Synchronous) -> sqlite3.Connection:
	create_database(path, schema, synchronous)

	uri = format_uri(os.fspath(path), 'rw' if writable else 'ro')
	db = sqlite3.connect(uri, uri=True, detect_types=sqlite3.PARSE_DECLTYPES, isolation_level=None, factory=Connection)

	try:
		db.row_factory = sqlite3.Row
		configure_database(db, synchronous)
	except Exception:
		db.close()
		raise

	return db


def create_database(path: typing.Union[str, os.PathLike[str]], schema: str, synchronous: Synchronous) -> None:
	try:
		fd = os.open(path, os.O_RDONLY | os.O_CREAT | os.O_EXCL | os.O_CLOEXEC, 0o660)
	except FileExistsError:
		return
	else:
		os.close(fd)

	LOGGER.info('Creating %s database at: %s', schema, path)

	uri = format_uri(os.fspath(path), 'rw')
	with contextlib.closing(sqlite3.connect(uri, uri=True, isolation_level=None, factory=Connection)) as db:
		configure_database(db, synchronous)
		with open_schema(schema) as fp:
			db.executescript(fp.read())


def open_media_database(cfg: Configuration, writable: bool = False) -> sqlite3.Connection:
	return open_database(cfg.database.media, 'media', writable, cfg.database.synchronous)


def open_reference_database(cfg: Configuration, writable: bool = False) -> sqlite3.Connection:
	return open_database(cfg.database.reference, 'reference', writable, cfg.database.synchronous)
