import dataclasses
import enum
import os
import pathlib
import shutil
import tomllib
import typing

from .logging import LOGGER

T = typing.TypeVar('T')

SENTINEL = object()


def _get(d: dict[str, typing.Any], k: str, t: typing.Callable[[typing.Any], T]) -> T:
	return t(d[k])


def _get_optional(d: dict[str, typing.Any], k: str, t: typing.Callable[[typing.Any], T]) -> typing.Optional[T]:
	v: typing.Union[T, object] = d.get(k, SENTINEL)
	return None if v is SENTINEL else t(v)


def _get_default(d: dict[str, typing.Any], k: str, t: typing.Callable[[typing.Any], T], f: T) -> T:
	v: typing.Union[T, object] = d.get(k, SENTINEL)
	return f if v is SENTINEL else t(v)


def _fix_path(p: pathlib.Path, r: typing.Optional[pathlib.Path] = None) -> pathlib.Path:
	if not p.is_absolute():
		if r is None:
			r = pathlib.Path.cwd()
		p = r / p
	try:
		p = p.resolve()
	except Exception:
		pass
	return p


def _make_tuple(l: list[typing.Any], t: typing.Type[T], x: typing.Optional[typing.Callable[[T], T]] = None) -> tuple[T, ...]:
	l2: typing.Iterator[T] = map(t, l)
	if x is not None:
		l2 = map(x, l2)
	return tuple(l2)


def _make_set(l: list[typing.Any], t: typing.Type[T], x: typing.Optional[typing.Callable[[T], T]] = None) -> frozenset[T]:
	l2: typing.Iterator[T] = map(t, l)
	if x is not None:
		l2 = map(x, l2)
	return frozenset(l2)


@dataclasses.dataclass(frozen=True)
class Directory:
	root: pathlib.Path
	database: pathlib.Path
	thumbnail: pathlib.Path

	@classmethod
	def parse(cls, data: typing.Any) -> typing.Self:
		return cls(
			_fix_path(_get_default(data, 'root', pathlib.Path, pathlib.Path('/'))),
			_fix_path(_get_default(data, 'database', pathlib.Path, pathlib.Path('/var/blackstar/database'))),
			_fix_path(_get_default(data, 'thumbnail', pathlib.Path, pathlib.Path('/var/blackstar/thumbnail'))),
		)


@dataclasses.dataclass(frozen=True)
class Device:
	mapping: dict[pathlib.Path, int]

	@classmethod
	def parse(cls, data: typing.Any, root: typing.Optional[pathlib.Path] = None) -> typing.Self:
		return cls({_fix_path(pathlib.Path(k), root): int(v) for k, v in data.items()})


@dataclasses.dataclass(frozen=True)
class Extension:
	image: frozenset[str]
	audio: frozenset[str]
	video: frozenset[str]
	linked: str

	@classmethod
	def parse(cls, data: typing.Any) -> typing.Self:
		return cls(
			_make_set(data.get('image', []), str, str.lower),
			_make_set(data.get('audio', []), str, str.lower),
			_make_set(data.get('video', []), str, str.lower),
			_get_default(data, 'linked', str, 'folder.jpg'),
		)


@dataclasses.dataclass(frozen=True)
class Binary:
	ffprobe: pathlib.Path
	ffmpeg: pathlib.Path
	exiftool: pathlib.Path
	timeout: typing.Optional[float]

	@classmethod
	def parse(cls, data: typing.Any) -> typing.Self:
		false = shutil.which('false') or '/bin/false'
		return cls(
			_fix_path(_get_default(data, 'ffprobe', pathlib.Path, pathlib.Path(shutil.which('ffprobe') or false))),
			_fix_path(_get_default(data, 'ffmpeg', pathlib.Path, pathlib.Path(shutil.which('ffmpeg') or false))),
			_fix_path(_get_default(data, 'exiftool', pathlib.Path, pathlib.Path(shutil.which('exiftool') or false))),
			_get_optional(data, 'timeout', float),
		)


class Synchronous(enum.StrEnum):
	OFF = 'off'
	NORMAL = 'normal'
	FULL = 'full'
	EXTRA = 'extra'


@dataclasses.dataclass(frozen=True)
class Database:
	media: pathlib.Path
	reference: pathlib.Path
	synchronous: Synchronous

	@classmethod
	def parse(cls, data: typing.Any, root: typing.Optional[pathlib.Path] = None) -> typing.Self:
		return cls(
			_fix_path(_get_default(data, 'media', pathlib.Path, pathlib.Path('media.sqlite')), root),
			_fix_path(_get_default(data, 'reference', pathlib.Path, pathlib.Path('reference.sqlite')), root),
			_get_default(data, 'synchronous', Synchronous, Synchronous.NORMAL),
		)


@dataclasses.dataclass(frozen=True)
class Server:
	name: typing.Optional[str]

	@classmethod
	def parse(cls, data: typing.Any) -> typing.Self:
		return cls(_get_optional(data, 'name', str))


@dataclasses.dataclass(frozen=True)
class Authentication:
	parameter: str
	cookie: str
	secret: bytes
	expiration: int

	@classmethod
	def parse(cls, data: typing.Any) -> typing.Self:
		return cls(
			_get_default(data, 'parameter', str, 'token'),
			_get_default(data, 'cookie', str, 'token'),
			_get_default(data, 'secret', str, '').encode('iso8859-1'),
			_get_default(data, 'expiration', int, 86400),
		)


@dataclasses.dataclass(frozen=True)
class Download:
	prefix: typing.Optional[str]
	private: typing.Optional[int]

	@classmethod
	def parse(cls, data: typing.Any) -> typing.Self:
		return cls(_get_optional(data, 'prefix', str), _get_optional(data, 'private', int))


@dataclasses.dataclass(frozen=True)
class Hidden:
	directory: frozenset[str]
	file: frozenset[str]
	linked: bool

	@classmethod
	def parse(cls, data: typing.Any) -> typing.Self:
		return cls(_make_set(data.get('directory', []), str), _make_set(data.get('file', []), str), _get_default(data, 'linked', bool, False))


@dataclasses.dataclass(frozen=True)
class ReadMe:
	markdown: tuple[str, ...]
	plain: tuple[str, ...]

	@classmethod
	def parse(cls, data: typing.Any) -> typing.Self:
		return cls(_make_tuple(data.get('markdown', []), str), _make_tuple(data.get('plain', []), str))


@dataclasses.dataclass(frozen=True)
class User:
	username: str
	password: str  # hash
	sequence: int
	email: typing.Optional[str]
	avatar: typing.Optional[str]
	root: pathlib.Path

	can_browse: bool
	can_download: bool
	can_share: bool
	can_xray: bool
	can_info: bool

	@classmethod
	def parse(cls, username: str, data: typing.Any, root: typing.Optional[pathlib.Path] = None) -> typing.Self:
		return cls(
			str(username),
			_get_default(data, 'password', str, ''),
			_get_default(data, 'sequence', int, 0),
			_get_optional(data, 'email', str),
			_get_optional(data, 'avatar', str),
			_fix_path(_get_default(data, 'root', pathlib.Path, pathlib.Path()), root),
			_get_default(data, 'can_browse', bool, True),
			_get_default(data, 'can_download', bool, True),
			_get_default(data, 'can_share', bool, False),
			_get_default(data, 'can_xray', bool, False),
			_get_default(data, 'can_info', bool, False),
		)


class Users(dict[str, User]):
	@classmethod
	def parse(cls, data: typing.Any, root: typing.Optional[pathlib.Path] = None) -> typing.Self:
		users = {}
		for username, value in data.items():
			users[username] = User.parse(username, value, root)
		return cls(users)


@dataclasses.dataclass(frozen=True)
class Configuration:
	directory: Directory
	device: Device
	extension: Extension
	binary: Binary
	database: Database
	server: Server
	authentication: Authentication
	download: Download
	hidden: Hidden
	readme: ReadMe
	users: Users

	@classmethod
	def parse(cls, data: typing.Any) -> typing.Self:
		empty: dict[str, typing.Any] = {}
		directory = Directory.parse(data.get('directory', empty))
		device = Device.parse(data.get('device', empty), directory.root)
		extension = Extension.parse(data.get('extension', empty))
		binary = Binary.parse(data.get('binary', empty))
		database = Database.parse(data.get('database', empty), directory.database)
		server = Server.parse(data.get('server', empty))
		authentication = Authentication.parse(data.get('authentication', empty))
		download = Download.parse(data.get('download', empty))
		hidden = Hidden.parse(data.get('hidden', empty))
		readme = ReadMe.parse(data.get('readme', empty))
		users = Users.parse(data.get('user', empty), directory.root)
		return cls(directory, device, extension, binary, database, server, authentication, download, hidden, readme, users)


def load(path: typing.Union[str, os.PathLike[str]]) -> Configuration:
	fspath = os.fspath(path)
	with open(fspath, 'rb') as fp:
		data = tomllib.load(fp)
	result = Configuration.parse(data)
	LOGGER.info('Loaded configuration from: %s', fspath)
	return result
