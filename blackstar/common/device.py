import os
import pathlib
import typing

from .configuration import Configuration
from .logging import LOGGER
from .node import Node


class DeviceMapper:
	def __init__(self, configuration: typing.Optional[Configuration] = None) -> None:
		self.root_node: typing.Optional[Node] = None
		self.logical_to_physical: dict[int, int] = {}
		self.physical_to_logical: dict[int, int] = {}

		if configuration is not None:
			self.fill(configuration)

	def fill(self, configuration: Configuration) -> None:
		self.logical_to_physical.clear()
		self.physical_to_logical.clear()

		self.root_node = self.add(configuration.directory.root, 0)
		for path, logical in configuration.device.mapping.items():
			self.add(path, logical)

	def add(self, path: pathlib.Path, logical: int) -> typing.Optional[Node]:
		try:
			st = os.lstat(path)
			physical = st.st_dev
		except Exception as e:
			LOGGER.error('Failed to determine physical device ID: %s', path, exc_info=e)
			return None

		if logical in self.logical_to_physical:
			LOGGER.error('Duplicate logical device ID: [%d]:%s', logical, path)
			return None

		if physical in self.physical_to_logical:
			LOGGER.error('Duplicate physical device ID: 0x%08x:%s', physical, path)
			return None

		self.logical_to_physical[logical] = physical
		self.physical_to_logical[physical] = logical

		LOGGER.info('Mapped device ID [%d]:0x%08x:%s', logical, physical, path)

		return Node(logical, st.st_ino)

	def resolve(self, st: os.stat_result) -> typing.Optional[Node]:
		logical = self.physical_to_logical.get(st.st_dev)
		if logical is None:
			LOGGER.warning('Unmapped physical device ID: 0x%08x', st.st_dev)
			return None
		return Node(logical, st.st_ino)
