import contextlib
import dataclasses
import functools
import pathlib
import sqlite3
import typing

from .uniqueid import generate

ID_LENGTH = 12
generate_id = functools.partial(generate, ID_LENGTH)


@dataclasses.dataclass(frozen=True)
class Reference:
	id: str
	expiration: typing.Optional[int]
	token: typing.Optional[str]
	path: pathlib.PurePosixPath
	download: bool


def get_reference(db: sqlite3.Connection, id: str) -> typing.Optional[Reference]:
	row = db.execute('SELECT expiration, token, path, download FROM reference WHERE id = ?', (id,)).fetchone()
	if row is None:
		return None
	return Reference(id, row['expiration'], row['token'], row['path'], bool(row['download']))


def get_reference_specific(
	db: sqlite3.Connection, expiration: typing.Optional[int], token: typing.Optional[str], path: pathlib.PurePosixPath, download: bool
) -> typing.Optional[Reference]:
	row = db.execute(
		'SELECT id FROM reference WHERE expiration IS ? AND token IS ? AND path == ? AND download == ?',
		(expiration, token, path, 1 if download else 0),
	).fetchone()
	if row is None:
		return None
	return Reference(row['id'], expiration, token, path, download)


def get_or_create_reference(
	db: sqlite3.Connection, expiration: typing.Optional[int], token: typing.Optional[str], path: pathlib.PurePosixPath, download: bool
) -> Reference:
	with contextlib.ExitStack() as stack:
		return get_or_create_reference_oplock(stack, db, expiration, token, path, download)
	assert False


def get_or_create_reference_oplock(
	stack: contextlib.ExitStack,
	db: sqlite3.Connection,
	expiration: typing.Optional[int],
	token: typing.Optional[str],
	path: pathlib.PurePosixPath,
	download: bool,
) -> Reference:
	reference = get_reference_specific(db, expiration, token, path, download)
	if reference is not None:
		return reference

	if not db.in_transaction:
		stack.enter_context(db)

		reference = get_reference_specific(db, expiration, token, path, download)
		if reference is not None:
			return reference

	id = generate_id()
	db.execute('INSERT INTO reference (id, expiration, token, path, download) VALUES (?,?,?,?,?)', (id, expiration, token, path, int(download)))
	return Reference(id, expiration, token, path, download)
