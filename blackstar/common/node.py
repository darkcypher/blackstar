import dataclasses


@dataclasses.dataclass(frozen=True, order=True)
class Node:
	device: int
	inode: int

	def __str__(self) -> str:
		return '{}:{}'.format(self.device, self.inode)
