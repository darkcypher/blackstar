import itertools
import random

RANDOMIZER = random.SystemRandom()
SYMBOLS = ''.join(map(chr, itertools.chain(range(0x30, 0x3A), range(0x41, 0x5B), range(0x61, 0x7B))))


def generate(length: int) -> str:
	return ''.join(RANDOMIZER.choices(SYMBOLS, k=length))
