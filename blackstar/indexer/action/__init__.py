import typing

from ..context import Context
from ..logging import LOGGER
from ..signal import checkpoint
from .base import Action
from .clean.metadata import CleanMetadataAction
from .clean.reference import CleanReferenceAction
from .clean.thumbnail import CleanThumbnailAction
from .scan import ScanAction

ACTIONS: list[typing.Type[Action]] = [ScanAction, CleanMetadataAction, CleanReferenceAction, CleanThumbnailAction]


def execute_actions(context: Context) -> None:
	return execute_actions_ex(context, ACTIONS)


def execute_actions_ex(context: Context, action_types: list[typing.Type[Action]]) -> None:
	for action_type in action_types:
		execute_action(context, action_type)


def execute_action(context: Context, action_type: typing.Type[Action]) -> None:
	LOGGER.info('Executing action: %s', action_type.name)
	action = action_type(context)
	action.checkpoint = checkpoint  # type: ignore[method-assign]
	action.execute()
