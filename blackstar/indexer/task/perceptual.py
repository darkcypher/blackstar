import os
import subprocess
import typing

import numpy
import scipy.fft

from ...common.node import Node
from ..context import Context
from ..external import try_execute
from ..logging import LOGGER
from .ffmpeg import FFMpegTask


class PerceptualTask(FFMpegTask):
	name = 'perceptual'
	description = 'Calculating perceptual hash'

	def __init__(self, context: Context, rowid: int, node: Node) -> None:
		super().__init__(context, rowid, node)
		self.perceptual: typing.Optional[int] = None

	def execute(self) -> None:
		path = self.get_path()
		if path is None:
			return

		args = self.ffmpeg()

		if self.seek is not None:
			args.extend(['-ss', str(round(self.seek, 3))])

		args.extend(
			[
				'-an',
				'-sn',
				'-dn',
				'-i',
				os.fspath(path),
				'-map_metadata',
				'-1',
				'-filter:v',
				'format=gray,scale=64:64',
				'-frames:v',
				'1',
				'-codec:v',
				'rawvideo',
				'-f',
				'image2pipe',
				'-',
			]
		)

		process = try_execute(args, stdout=subprocess.PIPE, check_stdout=True, timeout=self.context.configuration.binary.timeout)
		if process is None:
			return

		if len(process.stdout) != 64 * 64:
			LOGGER.warning('Image data too small (%d bytes)', len(process.stdout))
			return

		data = numpy.frombuffer(typing.cast(bytes, process.stdout), dtype=numpy.uint8).reshape(64, 64) / 255.0
		dct = scipy.fft.dctn(data)[:8, :8]
		median = numpy.median(dct)
		bits = (dct > median).flatten()
		self.perceptual = int(numpy.packbits(bits, bitorder='little').view('<i8')[0])

	def update_locked(self) -> None:
		self.context.database.execute(
			'UPDATE metadata SET perceptual = ? WHERE device = ? AND inode = ?', (self.perceptual, self.node.device, self.node.inode)
		)
		super().update_locked()
