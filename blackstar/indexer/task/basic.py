import os
import subprocess
import typing

from ...common.node import Node
from ..context import Context
from ..external import try_execute
from ..utility import try_convert, try_convert_ratio
from .ffmpeg import FFMpegTask
from .path import PathTask


class BasicTask(PathTask):
	name = 'basic'
	description = 'Extracting basic metadata'

	def __init__(self, context: Context, rowid: int, node: Node) -> None:
		super().__init__(context, rowid, node)
		self.width: typing.Optional[float] = None
		self.height: typing.Optional[float] = None
		self.duration: typing.Optional[float] = None
		self.frame_rate: typing.Optional[float] = None
		self.video_codec: typing.Optional[str] = None
		self.video_rate: typing.Optional[int] = None
		self.audio_codec: typing.Optional[str] = None
		self.audio_rate: typing.Optional[int] = None

	def prepare(self) -> None:
		super().prepare()
		self.paths = FFMpegTask.filter(self.paths)

	def execute(self) -> None:
		path = self.get_path()
		if path is None:
			return

		args = [
			os.fspath(self.context.configuration.binary.ffprobe),
			'-v',
			'quiet',
			'-print_format',
			'json',
			'-show_entries',
			'format=duration:stream=codec_type,codec_name,bit_rate,avg_frame_rate,width,height',
			'-i',
			os.fspath(path),
		]

		process = try_execute(args, stdout=subprocess.PIPE, encoding='json', check_stdout=True, timeout=self.context.configuration.binary.timeout)
		if process is None or not isinstance(process.json, dict):
			return

		empty0: dict[str, object] = {}
		empty1: list[object] = []

		def check_dict(value: object) -> dict[str, object]:
			return value if isinstance(value, dict) else empty0

		def check_list(value: object) -> list[object]:
			return value if isinstance(value, list) else empty1

		info: dict[str, object] = process.json
		video_stream: dict[str, object] = empty0
		audio_stream: dict[str, object] = empty0

		for stream in check_list(info.get('streams')):
			stream = check_dict(stream)
			codec_type = stream.get('codec_type')
			if codec_type == 'video' and video_stream is empty0:
				video_stream = stream
			elif codec_type == 'audio' and audio_stream is empty0:
				audio_stream = stream

		self.width = try_convert(int, video_stream.get('width'))
		self.height = try_convert(int, video_stream.get('height'))
		self.duration = try_convert(float, check_dict(info.get('format')).get('duration'))
		self.frame_rate = try_convert_ratio(video_stream.get('avg_frame_rate'))
		self.video_codec = try_convert(str, video_stream.get('codec_name'))
		self.video_rate = try_convert(int, video_stream.get('bit_rate'))
		self.audio_codec = try_convert(str, audio_stream.get('codec_name'))
		self.audio_rate = try_convert(int, audio_stream.get('bit_rate'))

	def update_locked(self) -> None:
		self.context.database.execute(
			'UPDATE metadata SET width = ?, height = ?, duration = ?, frame_rate = ?, video_codec = ?, video_rate = ?, audio_codec = ?, audio_rate = ? WHERE device = ? AND inode = ?',
			(
				self.width,
				self.height,
				self.duration,
				self.frame_rate,
				self.video_codec,
				self.video_rate,
				self.audio_codec,
				self.audio_rate,
				self.node.device,
				self.node.inode,
			),
		)
		super().update_locked()
