from ...common.node import Node
from ..context import Context
from .base import Task


class BatchTask(Task):
	batch = True

	def __init__(self, context: Context, rowids: list[int], nodes: list[Node]) -> None:
		super().__init__(context)
		self.rowids = rowids
		self.nodes = nodes

	def update(self) -> None:
		with self.context.database:
			self.update_locked()

	def update_locked(self) -> None:
		self.context.database.executemany('DELETE FROM queue WHERE ROWID = ?', ((rowid,) for rowid in self.rowids))
