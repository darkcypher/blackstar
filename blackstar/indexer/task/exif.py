import os
import subprocess
import typing

from ...common.node import Node
from ..context import Context
from ..external import try_execute
from ..utility import try_convert
from .path import PathTask


class ExifTask(PathTask):
	name = 'exif'
	description = 'Extracting EXIF metadata'

	def __init__(self, context: Context, rowid: int, node: Node) -> None:
		super().__init__(context, rowid, node)
		self.exif_make: typing.Optional[str] = None
		self.exif_model: typing.Optional[str] = None
		self.exif_software: typing.Optional[str] = None
		self.exif_latitude: typing.Optional[float] = None
		self.exif_longitude: typing.Optional[float] = None

	def execute(self) -> None:
		path = self.get_path()
		if path is None:
			return

		args = [
			os.fspath(self.context.configuration.binary.exiftool),
			'-n',
			'-json',
			'-Make',
			'-Model',
			'-Software',
			'-GPSLatitude',
			'-GPSLongitude',
			'--',
			os.fspath(path),
		]

		process = try_execute(args, stdout=subprocess.PIPE, encoding='json', check_stdout=True, timeout=self.context.configuration.binary.timeout)
		if process is None or not isinstance(process.json, list) or not len(process.json) or not isinstance(process.json[0], dict):
			return

		data: dict[str, object] = process.json[0]

		self.exif_make = try_convert(str, data.get('Make')) or None
		self.exif_model = try_convert(str, data.get('Model')) or None
		self.exif_software = try_convert(str, data.get('Software')) or None
		self.exif_latitude = try_convert(float, data.get('GPSLatitude')) or None
		self.exif_longitude = try_convert(float, data.get('GPSLongitude')) or None

	def update_locked(self) -> None:
		self.context.database.execute(
			'UPDATE metadata SET exif_make = ?, exif_model = ?, exif_software = ?, exif_latitude = ?, exif_longitude = ? WHERE device = ? AND inode = ?',
			(self.exif_make, self.exif_model, self.exif_software, self.exif_latitude, self.exif_longitude, self.node.device, self.node.inode),
		)
		super().update_locked()
