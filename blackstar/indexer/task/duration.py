import typing

from ...common.node import Node
from ..context import Context
from .single import SingleTask


class DurationTask(SingleTask):
	def __init__(self, context: Context, rowid: int, node: Node) -> None:
		super().__init__(context, rowid, node)
		self.duration: typing.Optional[float] = None
		self.seek: typing.Optional[float] = None

	def prepare(self) -> None:
		super().prepare()

		row = self.context.database.execute(
			"SELECT duration FROM metadata WHERE device = ? AND inode = ? AND type = 'V'", (self.node.device, self.node.inode)
		).fetchone()
		if row is None:
			return

		duration = row['duration']
		if duration is None:
			return

		self.duration = duration
		self.seek = duration / 3.0
