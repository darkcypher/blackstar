import os
import pathlib
import stat
import typing

from ...common.lookup import get_paths
from ...common.node import Node
from ..context import Context
from ..logging import LOGGER
from ..utility import try_fstat, try_lstat, try_open_file
from .single import SingleTask


class PathTask(SingleTask):
	def __init__(self, context: Context, rowid: int, node: Node) -> None:
		super().__init__(context, rowid, node)
		self.paths: list[pathlib.Path] = []
		self.metadata_size: typing.Optional[int] = None
		self.metadata_mtime: typing.Optional[float] = None

	def prepare(self) -> None:
		super().prepare()

		row = self.context.database.execute(
			'SELECT size, mtime FROM metadata WHERE device = ? AND inode = ?', (self.node.device, self.node.inode)
		).fetchone()
		if row is not None:
			self.metadata_size = row['size']
			self.metadata_mtime = row['mtime']

		root_path = self.context.configuration.directory.root

		paths_pure = get_paths(self.context.database, self.context.device_mapper.root_node, self.node)
		self.paths = [root_path / path_pure for path_pure in paths_pure]

		if paths_pure:
			self.extra = paths_pure[0]

	def get_path(self) -> typing.Optional[pathlib.Path]:
		for path in self.paths:
			st = try_lstat(path)
			if self.check_node(st):
				return path
		return None

	def get_fd(self) -> typing.Optional[int]:
		for path in self.paths:
			fd = try_open_file(path)
			if fd is None:
				continue
			st = try_fstat(fd)
			if self.check_node(st):
				return fd
			os.close(fd)
		return None

	def check_node(self, st: typing.Optional[os.stat_result]) -> bool:
		if st is None:
			return False
		if not stat.S_ISREG(st.st_mode):
			return False
		resolved = self.context.device_mapper.resolve(st)
		if resolved is None:
			return False
		if resolved != self.node:
			LOGGER.warning('Mismatched node on filesystem: %s != %s', resolved, self.node)
			return False
		if st.st_size != self.metadata_size:
			LOGGER.warning('Mismatched size with metadata: %d != %d', st.st_size, self.metadata_size)
			return False
		if st.st_mtime != self.metadata_mtime:
			LOGGER.warning('Mismatched time with metadata: %f != %f', st.st_mtime, self.metadata_mtime)
			return False
		return True
