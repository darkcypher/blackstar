import os
import subprocess
import typing

from ...common.node import Node
from ...common.thumbnail import create_new
from ..context import Context
from ..external import try_execute
from .ffmpeg import FFMpegTask


class Preview1xTask(FFMpegTask):
	name = 'preview_1x'
	description = 'Rendering preview @1x'
	size = 128
	count = 12
	length = 0.75
	column = name

	def __init__(self, context: Context, rowid: int, node: Node) -> None:
		super().__init__(context, rowid, node)
		self.preview: typing.Optional[str] = None

	def execute(self) -> None:
		path = self.get_path()
		if path is None:
			return

		if self.duration is None:
			return

		preview, fd, preview_path = create_new(self.context.configuration, True)
		os.close(fd)

		args = self.ffmpeg()

		threshold = self.count * self.length * 1.1

		filter_argument = '-filter:v'
		filter_value = 'scale={0}:{0}:force_original_aspect_ratio=decrease:force_divisible_by=2,setsar=1,setdar=a,format=yuv420p'.format(self.size)

		if self.duration <= threshold:
			args.extend(['-t', str(round(threshold, 3)), '-an', '-sn', '-dn', '-i', os.fspath(path)])
		else:
			for i in range(0, self.count):
				middle = (self.duration / (self.count + 2.0)) * (i + 1.0)
				start = middle - (self.length / 2.0)
				args.extend(['-ss', str(round(start, 3)), '-t', str(round(self.length, 3)), '-an', '-sn', '-dn', '-i', os.fspath(path)])

			filter_argument = '-filter_complex'
			filter_value = '{}concat={},{}'.format(''.join('[{}:v]'.format(i) for i in range(0, self.count)), self.count, filter_value)

		args.extend(
			[
				'-map_metadata',
				'-1',
				filter_argument,
				filter_value,
				'-codec:v',
				'libx264',
				'-profile:v',
				'baseline',
				'-y',
				'--',
				os.fspath(preview_path),
			]
		)

		process = try_execute(args, stdout=subprocess.DEVNULL, check_path=preview_path, timeout=self.context.configuration.binary.timeout)
		if process is None:
			os.unlink(preview_path)
			return

		self.preview = preview

	def update_locked(self) -> None:
		self.context.database.execute(
			'UPDATE metadata SET {} = ? WHERE device = ? AND inode = ?'.format(self.column), (self.preview, self.node.device, self.node.inode)
		)
		super().update_locked()


class Preview2xTask(Preview1xTask):
	name = 'preview_2x'
	description = 'Rendering preview @2x'
	size = Preview1xTask.size * 2
	column = name
