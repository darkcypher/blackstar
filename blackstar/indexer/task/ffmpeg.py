import os
import pathlib

from .duration import DurationTask
from .path import PathTask


class FFMpegTask(PathTask, DurationTask):
	name = 'ffmpeg'

	@staticmethod
	def filter(paths: list[pathlib.Path]) -> list[pathlib.Path]:
		return [path for path in paths if path.suffix.lower() != '.vtx']

	def prepare(self) -> None:
		super().prepare()
		self.paths = self.filter(self.paths)

	def ffmpeg(self) -> list[str]:
		args = [os.fspath(self.context.configuration.binary.ffmpeg), '-hide_banner', '-nostats', '-loglevel', 'warning']

		if self.context.pool is not None:
			args.extend(['-threads', '1', '-filter_threads', '1'])

		return args
