import abc
import typing

from ..action import Action
from ..context import Context
from ..logging import LOGGER


class Task(Action):
	name: str = 'task'
	batch: typing.Optional[bool] = None

	description: typing.Optional[str] = None
	extra: object = None

	def __init__(self, context: Context) -> None:
		super().__init__(context)

	@classmethod
	def initialize(cls, context: Context) -> None:
		pass

	def prepare(self) -> None:
		pass

	def announce(self) -> None:
		if self.description is None:
			return

		if self.extra is None:
			LOGGER.info('%s', self.description)
		else:
			LOGGER.info('%s: %s', self.description, self.extra)

	@abc.abstractmethod
	def execute(self) -> None:
		pass

	@abc.abstractmethod
	def update(self) -> None:
		pass

	@classmethod
	def finalize(cls, context: Context) -> None:
		pass
