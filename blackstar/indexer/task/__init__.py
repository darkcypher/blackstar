import typing

from ..context import Context
from .base import Task
from .basic import BasicTask
from .executor import execute_tasks as execute_tasks_ex
from .exif import ExifTask
from .hash import HashTask
from .perceptual import PerceptualTask
from .preview import Preview1xTask, Preview2xTask
from .similar import SimilarImageTask, SimilarVideoTask
from .thumbnail import Thumbnail1xTask, Thumbnail2xTask, Thumbnail3xTask, Thumbnail4xTask

TASKS: list[typing.Type[Task]] = [
	BasicTask,
	ExifTask,
	Thumbnail2xTask,
	Thumbnail1xTask,
	Preview2xTask,
	Preview1xTask,
	Thumbnail4xTask,
	Thumbnail3xTask,
	PerceptualTask,
	SimilarImageTask,
	SimilarVideoTask,
	HashTask,
]


def execute_tasks(context: Context) -> None:
	return execute_tasks_ex(context, TASKS)
