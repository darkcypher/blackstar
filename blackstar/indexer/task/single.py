from ...common.node import Node
from ..context import Context
from .base import Task


class SingleTask(Task):
	batch = False

	def __init__(self, context: Context, rowid: int, node: Node) -> None:
		super().__init__(context)
		self.extra = node
		self.rowid = rowid
		self.node = node

	def update(self) -> None:
		with self.context.database:
			self.update_locked()

	def update_locked(self) -> None:
		self.context.database.execute('DELETE FROM queue WHERE ROWID = ?', (self.rowid,))
