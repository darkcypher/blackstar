import typing

import numpy
import numpy.typing

from ...common.node import Node
from ..context import Context
from .batch import BatchTask


class SimilarTask(BatchTask):
	name = 'similar'
	type = ''
	threshold = 8

	def __init__(self, context: Context, rowids: list[int], nodes: list[Node]) -> None:
		super().__init__(context, rowids, nodes)
		self.everything: list[tuple[Node, int]] = []
		self.cutoff: typing.Optional[int] = None
		self.hashes: typing.Optional[numpy.typing.NDArray[numpy.uint64]] = None
		self.similar: list[tuple[Node, Node, int]] = []

	def prepare(self) -> None:
		previous: typing.Optional[bool] = None
		for i, row in enumerate(
			self.context.database.execute(
				'SELECT EXISTS(SELECT * FROM queue WHERE queue.device = metadata.device AND queue.inode = metadata.inode AND task = ?) AS new, device, inode, perceptual FROM metadata WHERE type = ? AND perceptual IS NOT NULL ORDER BY new, device, inode',
				(self.name, self.type),
			)
		):
			self.checkpoint()

			new = row['new']
			if new and not previous:
				self.cutoff = i
				previous = new

			self.everything.append((Node(row['device'], row['inode']), row['perceptual']))

	def execute(self) -> None:
		if self.cutoff is None:
			return

		self.hashes = numpy.array([x[1] for x in self.everything], dtype=numpy.int64).view(numpy.uint64)

		r = range(self.cutoff, len(self.everything))
		if self.context.pool is None:
			for i in r:
				result = self.execute_index(i)
				if result is not None:
					self.similar.extend(result)
		else:
			for result in self.context.pool.imap_unordered(self.execute_index, r):
				if result is not None:
					self.similar.extend(result)

	def execute_index(self, i: int) -> typing.Optional[list[tuple[Node, Node, int]]]:
		self.checkpoint()
		if self.hashes is None:
			return None

		distances = numpy.bitwise_count(self.hashes[:i] ^ self.hashes[i])
		indices = numpy.nonzero(distances < self.threshold)[0]

		if not indices.size:
			return None

		result: list[tuple[Node, Node, int]] = []
		node0 = self.everything[i][0]
		for j in indices:
			node1 = self.everything[j][0]
			distance = int(distances[j])

			result.append((node0, node1, distance))
			result.append((node1, node0, distance))

		return result

	def update_locked(self) -> None:
		self.context.database.executemany(
			'INSERT INTO similar (device_source, inode_source, device_target, inode_target, distance) VALUES (?, ?, ?, ?, ?)',
			((source.device, source.inode, target.device, target.inode, distance) for source, target, distance in self.similar),
		)
		super().update_locked()

	@classmethod
	def finalize(cls, context: Context) -> None:
		with context.database:
			context.database.execute(
				'UPDATE metadata SET count_similar = (SELECT COUNT(*) FROM similar WHERE similar.device_source = metadata.device AND similar.inode_source = metadata.inode) WHERE type = ?',
				(cls.type,),
			)


class SimilarImageTask(SimilarTask):
	name = 'similar_i'
	type = 'I'


class SimilarVideoTask(SimilarTask):
	name = 'similar_v'
	type = 'V'
