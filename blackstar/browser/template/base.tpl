<%
from bottle import request
from blackstar.browser.template import format_title

setdefault('title', None)
setdefault('stylesheet', ['basic'])
setdefault('script', [])
setdefault('center', False)
setdefault('base', '')
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="theme-color" content="#a040ff">
<title>{{ format_title(title) }}</title>
<link href="{{ request.app.get_url('/favicon.svg', pure=True) }}" type="image/svg+xml" rel="icon">
%for css in stylesheet:
<link href="{{ request.app.get_url('static', path='style/{}.css'.format(css), pure=True) }}" rel="stylesheet">
%end
%for js in script:
<script src="{{ request.app.get_url('static', path='script/{}.js'.format(js), pure=True) }}" defer></script>
%end
</head>
%if center:
<body class="center">
%else:
<body>
%end
<div class="container">
{{! base.rstrip('\n') }}
</div>
</body>
</html>
