<%
from bottle import request

rebase('header')
%>
<span class="buttons">
<button data-uri="{{ request.app.get_url('database') }}">
<span class="icon database"></span>
<span class="hidden-phone uwu">Database</span>
</button>
<button data-uri="{{ request.app.get_url('location') }}">
<span class="icon map-o"></span>
<span class="hidden-phone uwu">Location</span>
</button>
</span>
