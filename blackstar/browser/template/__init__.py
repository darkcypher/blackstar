import dataclasses
import re
import sys
import time
import typing

import bottle

from ..configuration import get_current as get_configuration


def fix_encoding(text: str) -> str:
	return text.encode(sys.getfilesystemencoding(), sys.getfilesystemencodeerrors()).decode('utf-8', 'replace')


def format_title(title: typing.Optional[str] = None, full: bool = False) -> str:
	if title and not full:
		return title

	cfg = get_configuration()
	server_name: str = cfg.server.name or bottle.request.environ.get('SERVER_NAME', 'blackstar')

	if title:
		return '{} | {}'.format(title, server_name)
	else:
		return server_name


FLAG_RE = re.compile('^[A-Z]{2}$')


def format_flag(country: typing.Optional[str] = None) -> str:
	if country is None:
		country = bottle.request.environ.get('HTTP_CF_IPCOUNTRY', 'XX')
	if country == 'T1':
		return '&#x1F3F4;&#x200D;&#x2620;&#xFE0F;'
	elif country == 'XX' or not FLAG_RE.match(country):
		return '&#x1F3F3;&#xFE0F;'
	else:
		return ''.join('&#x{:X};'.format(ord(c) - 0x41 + 0x1F1E6) for c in country)


@dataclasses.dataclass(frozen=True)
class SizeUnit:
	specifier: str
	threshold: float
	digits: int


SIZE_BLOCK = 1024.0
SIZE_UNIT = [
	SizeUnit('B', SIZE_BLOCK, 0),
	SizeUnit('K', SIZE_BLOCK, 0),
	SizeUnit('M', SIZE_BLOCK * 8.0, 1),
	SizeUnit('G', SIZE_BLOCK, 2),
	SizeUnit('T', SIZE_BLOCK, 2),
]


def format_size(size: typing.Optional[int]) -> str:
	if size is None:
		return '-'

	fsize = float(size)
	iterator = iter(SIZE_UNIT)
	while True:
		try:
			unit = next(iterator)
		except StopIteration:
			break

		if fsize < unit.threshold:
			break

		fsize = fsize / SIZE_BLOCK

	fmt = '{{:.{}f}}{{}}'.format(unit.digits)
	return fmt.format(fsize, unit.specifier)


DATE_FMT = '%Y-%m-%d %H:%M:%S'


def format_time(value: typing.Union[float, int, None]) -> str:
	if not value:
		return '-'

	return time.strftime(DATE_FMT, time.localtime(value))
