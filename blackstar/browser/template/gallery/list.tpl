<%
from bottle import request
from blackstar.browser.template import format_size, format_time
from blackstar.browser.gallery import Kind, get_small_icon, get_info

setdefault('entries', [])

rebase('gallery/base')
%>
<table class="striped">
<colgroup>
<col>
<col>
<col>
<col>
</colgroup>
<thead>
<tr class="hidden-phone uwu">
<th>Name</th>
<th><span class="hidden">Action</span></th>
<th>Info</th>
<th>Size</th>
<th>Time</th>
</tr>
</thead>
<tbody>
%for entry in entries:
<tr>
<td>
<span class="{{ ' '.join(x for x in ['icon', get_small_icon(entry.kind), 'ghost' if entry.hidden else None] if x) }}"></span>
%if entry.reference is None:
{{ entry.display }}
%else:
<a href="{{ request.app.get_url('dispatch', id=entry.reference.id) }}">{{ entry.display }}</a>
%end
%if entry.link:
<span class="icon ghost external-link-square"></span>
%end
<td>
%include('gallery/actions')
</td>
<td>{{ get_info(entry) }}</td>
<td>{{ format_size(entry.weight or entry.size) }}</td>
<td>{{ format_time(entry.mtime) }}</td>
</tr>
%end
</tbody>
</table>
