%from bottle import request
<nav class="path">
<h2 class="hidden">Path</h2>
<ol class="path">\\
%for i, fragment in enumerate(fragments):
<li><wbr>\\
%if i == len(fragments) - 1:
<strong>\\
%else:
<a href="{{ request.app.get_url('dispatch', id=fragment.reference.id) }}">\\
%end
%if i == 0:
<span class="icon home"></span><span class="hidden">ROOT</span>\\
%else:
{{ fragment.display }}\\
%end
%if i == len(fragments) - 1:
</strong>\\
%else:
<span class="separator">/</span></a>\\
%end
</li>\\
%end
</ol>
%if len(fragments) > 1:
<button class="minimal" data-copy-start="ol.path > li:nth-child(2)" data-copy-end="ol.path > li:last-child" title="copy"><span class="icon files-o"></span><span class="hidden uwu">Copy</span></button>
%end
</nav>
