<%
import hashlib
from bottle import request
from blackstar.browser.template import format_flag, format_time
from blackstar.browser.authorization import TokenSource

setdefault('title', None)
setdefault('base', '')
%>
<header class="header">
%if title:
<h1>{{ title }}</h1>
%end
%address = request.environ.get('HTTP_X_REAL_IP', request.environ.get('REMOTE_ADDR', ''))
<span class="flag">{{! format_flag() }}</span>
%if request.token.username:
%asz = 21
%if request.token.avatar:
<img class="avatar" src="{{ request.token.avatar }}" width="{{asz}}" height="{{asz}}">
%elif request.token.email:
%avatar = 'https://www.gravatar.com/avatar/' + hashlib.md5((request.token.email or '').encode('utf-8', errors='ignore')).hexdigest()
<img class="avatar" src="{{ avatar }}?s=21&d=mp" srcset="{{avatar}}?s={{asz}}&d=mp 1x, {{avatar}}?s={{asz*2}}&d=mp 2x, {{avatar}}?s={{asz*3}}&d=mp 3x, {{avatar}}?s={{asz*4}}&d=mp 4x" width="{{asz}}" height="{{asz}}">
%else:
<img class="avatar" src="{{ request.app.get_url('static', path='image/avatar.svg', pure=True) }}" width="{{asz}}" height="{{asz}}">
%end
<span class="username" title="{{ request.token.email or request.token.username }} ({{ address }})&#x000a;expiration @ {{ format_time(request.token.expiration) if request.token.expiration else 'never' }}">{{ request.token.username }}</span>
%else:
<code class="address" title="expiration @ {{ format_time(request.token.expiration) if request.token.expiration else 'never' }}">{{ address }}</code>
%end
{{! base.rstrip('\n') }}
%if request.token_source == TokenSource.COOKIE:
<button data-uri="{{ request.app.get_url('logout', pure=True) }}">
<span class="icon power-off"></span>
<span class="hidden-phone uwu">Logout</span>
</button>
%end
</header>
