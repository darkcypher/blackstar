import contextlib
import dataclasses
import os
import pathlib
import stat
import sys
import traceback
import typing
import urllib.parse

import bottle

from ..common.reference import get_or_create_reference_oplock as get_or_create_reference_raw_oplock
from ..common.reference import get_reference as get_reference_raw
from .authorization import TokenSource, decode_validate
from .configuration import get_current as get_configuration
from .database import get_reference_db
from .token import Token


@dataclasses.dataclass(frozen=True)
class Reference:
	id: str
	expiration: typing.Optional[int]
	token: typing.Optional[Token]
	path: pathlib.PurePosixPath
	download: bool


ReferenceCreator = typing.Callable[[typing.Optional[Token], pathlib.PurePosixPath, bool], Reference]

T1 = typing.TypeVar('T1')
T2 = typing.TypeVar('T2')


@dataclasses.dataclass(frozen=True)
class ResolvedReference:
	reference: Reference
	root: pathlib.Path
	target: pathlib.Path
	stat: os.stat_result

	@typing.overload
	def dispatch(self, /, file: None, directory: None) -> None: ...

	@typing.overload
	def dispatch(self, /, file: typing.Callable[[typing.Self], T1], directory: None) -> typing.Optional[T1]: ...

	@typing.overload
	def dispatch(self, /, file: None, directory: typing.Callable[[typing.Self], T2]) -> typing.Optional[T2]: ...

	@typing.overload
	def dispatch(self, /, file: typing.Callable[[typing.Self], T1], directory: typing.Callable[[typing.Self], T2]) -> typing.Union[T1, T2]: ...

	def dispatch(
		self,
		/,
		file: typing.Optional[typing.Callable[[typing.Self], T1]] = None,
		directory: typing.Optional[typing.Callable[[typing.Self], T2]] = None,
	) -> typing.Union[T1, T2, None]:
		if stat.S_ISREG(self.stat.st_mode):
			return None if file is None else file(self)
		elif stat.S_ISDIR(self.stat.st_mode):
			return None if directory is None else directory(self)
		else:
			raise RuntimeError('unexpected reference file type')


def get_reference(id: str) -> typing.Optional[Reference]:
	db = get_reference_db()
	raw = get_reference_raw(db, id)
	if raw is None:
		return None
	if raw.token is not None:
		token = decode_validate(raw.token, TokenSource.DATABASE)
		if token is None:
			return None
	else:
		token = None
	return Reference(raw.id, raw.expiration, token, raw.path, raw.download)


def get_or_create_reference(token: typing.Optional[Token], path: pathlib.PurePosixPath, download: bool) -> Reference:
	with contextlib.ExitStack() as stack:
		return get_or_create_reference_oplock(stack, token, path, download)
	assert False


def get_or_create_reference_oplock(
	stack: contextlib.ExitStack, token: typing.Optional[Token], path: pathlib.PurePosixPath, download: bool
) -> Reference:
	cfg = get_configuration()
	if token is None:
		expiration = None
		rtoken = None
	else:
		expiration = token.expiration
		rtoken = token.encode(cfg.authentication.secret)
	raw = get_or_create_reference_raw_oplock(stack, get_reference_db(), expiration, rtoken, path, download)
	return Reference(raw.id, raw.expiration, token, raw.path, raw.download)


def resolve_validate(id: str) -> ResolvedReference:
	reference = get_reference(id)
	if reference is None:
		raise bottle.HTTPError(404, 'Reference Not Found')

	token = bottle.request.token
	if reference.token is not None:
		token = reference.token

	if token is None:
		raise bottle.HTTPError(401, 'Reference Token Invalid or Missing')

	cfg = get_configuration()
	target = cfg.directory.root / reference.path

	try:
		target.resolve(True)
	except (FileNotFoundError, NotADirectoryError) as e:
		raise bottle.HTTPError(404, 'Reference Path Not Found', e, traceback.format_exc())
	except PermissionError as e:
		raise bottle.HTTPError(403, 'Reference Path Forbidden', e, traceback.format_exc())
	except Exception as e:
		raise bottle.HTTPError(500, 'Reference Path Error', e, traceback.format_exc())

	root = cfg.directory.root
	if token.root is not None:
		root_reference = get_reference(token.root)
		if root_reference is None:
			raise bottle.HTTPError(404, 'Root Reference Not Found')
		root = root / root_reference.path

	if not target.is_relative_to(root):
		raise bottle.HTTPError(403, 'Reference Path Outside Root')

	try:
		stat_buf = target.lstat()
	except (FileNotFoundError, NotADirectoryError) as e:
		raise bottle.HTTPError(404, 'Target Path Not Found', e, traceback.format_exc())
	except PermissionError as e:
		raise bottle.HTTPError(403, 'Target Path Forbidden', e, traceback.format_exc())

	return ResolvedReference(reference, root, target, stat_buf)


def update_environ(action: str, resolved: ResolvedReference) -> None:
	path = os.fspath(pathlib.PurePosixPath('/') / resolved.reference.path)
	value = '{}:{}'.format(action, urllib.parse.quote(path, encoding=sys.getfilesystemencoding(), errors=sys.getfilesystemencodeerrors()))
	bottle.request.environ['blackstar.extra'] = value


def make_pure_path(path: pathlib.Path) -> pathlib.PurePosixPath:
	cfg = get_configuration()
	relative = path.relative_to(cfg.directory.root)
	return pathlib.PurePosixPath(*relative.parts)
