import typing

import bottle

from .application import master


def error_handler(res: bottle.HTTPError) -> str:
	bottle.response.set_header('Cache-Control', 'no-store')
	return typing.cast(str, bottle.template('error', error=res))


master.default_error_handler = error_handler
