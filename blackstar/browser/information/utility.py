import dataclasses
import pathlib

import bottle

from ...common.lookup import get_paths
from ...common.node import Node
from ..configuration import get_current as get_configuration
from ..configuration import get_mapper
from ..database import get_media_db
from ..gallery import is_hidden
from ..reference import get_reference
from ..template import fix_encoding
from ..token import Token


@dataclasses.dataclass(frozen=True)
class Limiter:
	browsable: bool = False
	visible: bool = False
	root: pathlib.PurePosixPath = pathlib.PurePosixPath()


@dataclasses.dataclass(frozen=True)
class ResolvedNode:
	node: Node
	display: str
	names: list[str]
	paths: list[pathlib.PurePosixPath]


def get_limiter() -> Limiter:
	try:
		token: Token = bottle.request.token
	except AttributeError:
		return Limiter()

	browsable = bool(token.can_browse)
	visible = bool(token.can_xray)

	root_id = token.root
	if root_id is None:
		return Limiter(browsable, visible)

	root_reference = get_reference(root_id)
	if root_reference is None:
		return Limiter(False, visible)

	return Limiter(browsable, visible, root_reference.path)


def resolve_node(limiter: Limiter, node: Node) -> ResolvedNode:
	db = get_media_db()
	mapper = get_mapper()

	if limiter.browsable:
		paths = get_paths(db, mapper.root_node, node)

		if not limiter.visible:
			paths = [path for path in paths if not is_hidden_path(limiter, path)]

		names = sorted(set(fix_encoding(path.name) for path in paths))
	else:
		paths = []
		names = []

	if names:
		display = names[0]
	else:
		display = 'Node #{}'.format(node)

	return ResolvedNode(node, display, names, paths)


def is_hidden_path(limiter: Limiter, path: pathlib.PurePosixPath) -> bool:
	if not path.is_relative_to(limiter.root):
		return True

	cfg = get_configuration()
	if is_hidden(path.name, cfg.hidden.file):
		return True

	for part in path.parent.parts:
		if is_hidden(part, cfg.hidden.directory):
			return True

	return False
