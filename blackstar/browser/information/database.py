import dataclasses
import typing

import bottle

from ..application import master
from ..database import get_media_db
from ..template import format_size


@dataclasses.dataclass(frozen=True)
class Column:
	display: str
	formatter: typing.Callable[[typing.Any], str] = str
	visible: bool = dataclasses.field(default=True, kw_only=True)
	tooltip: bool = dataclasses.field(default=False, kw_only=True)
	lower: bool = dataclasses.field(default=False, kw_only=True)


@dataclasses.dataclass(frozen=True)
class Row:
	display: typing.Optional[str]
	values: list[typing.Any]
	ratio: typing.Optional[float] = None
	visible: bool = dataclasses.field(default=True, kw_only=True)
	dlower: typing.Optional[bool] = dataclasses.field(default=None, kw_only=True)


@dataclasses.dataclass(frozen=True)
class Data:
	columns: list[Column]
	rows: list[Row]

	def __bool__(self) -> bool:
		return bool(self.rows)


@master.route('/view/database', name='database', media=False)
def information_database() -> typing.Union[bottle.HTTPError, str]:
	if not bottle.request.token.can_info:
		return bottle.HTTPError(403, 'Information Restricted')

	bottle.response.set_header('Cache-Control', 'private, no-store')

	return bottle.template(
		'information/database',
		database=query_database_information(),
		task=query_task_information(),
		redundancy=query_redundancy_information(),
		video=query_video_codec_information('V'),
		image=query_video_codec_information('I'),
		audio=query_audio_codec_information(),
		resv=query_resolution_information('V'),
		resi=query_resolution_information('I'),
	)


def default_filter(index: int, count: int) -> typing.Literal[True]:
	return True


def query_data(
	columns: list[Column],
	sql: str,
	*parameters: object,
	display_column: typing.Optional[int] = None,
	value_column: typing.Optional[int] = None,
	ratio_filter: typing.Callable[[int, int], bool] = default_filter,
	ratio_threshold: float = 1.0 / 32.0,
	row_limit: typing.Optional[int] = None,
) -> Data:
	db = get_media_db()
	data: list[list[typing.Any]] = list(map(list, db.execute(sql, parameters)))

	total: typing.Optional[float] = None
	if value_column is not None:
		total = 0.0
		for i, values in enumerate(data):
			if ratio_filter(i, len(data)):
				total += float(values[value_column])

		if total <= 0.0:
			total = None

	dlower: typing.Optional[bool] = None
	if display_column is not None:
		dlower = columns[display_column].lower

	rows: list[Row] = []
	leftover = 0.0
	for i, values in enumerate(data):
		ratio: typing.Optional[float] = None
		if value_column is not None and total is not None:
			if ratio_filter(i, len(data)):
				ratio = float(values[value_column]) / total
				if ratio < ratio_threshold:
					leftover += ratio
					ratio = None

		display: typing.Optional[str] = None
		if display_column is not None:
			display = columns[display_column].formatter(values[display_column])

		visible = row_limit is None or i < row_limit

		rows.append(Row(display, values, ratio, visible=visible, dlower=dlower))

	if leftover > 0.0:
		rows.append(Row('Other', [None] * len(columns), leftover, visible=False, dlower=True))

	return Data(columns, rows)


def query_database_information() -> Data:
	types: dict[str, str] = {'D': 'Directory', 'F': 'File', 'I': 'Image', 'A': 'Audio', 'V': 'Video', 'Z': 'Total'}

	columns: list[Column] = [
		Column('Type', types.__getitem__, lower=True),
		Column('Count'),
		Column('Average', format_size, tooltip=True),
		Column('Total', format_size, tooltip=True),
	]

	return query_data(
		columns,
		'SELECT type, COUNT(*) AS count, '
		'CAST(AVG(size) AS INTEGER) AS average, '
		'CAST(TOTAL(size) AS INTEGER) AS total '
		'FROM metadata GROUP BY type UNION '
		"SELECT 'Z' AS type, COUNT(*) AS count, "
		'CAST(AVG(size) AS INTEGER) AS average, '
		'CAST(TOTAL(size) AS INTEGER) AS total '
		'FROM metadata',
		display_column=0,
		value_column=1,
		ratio_filter=lambda i, c: i < (c - 1),
	)


def query_task_information() -> Data:
	columns: list[Column] = [
		Column('Task', lambda task: typing.cast(str, task).replace('_', ' @')),
		Column('Count'),
		Column('Size', format_size, tooltip=True),
	]

	return query_data(
		columns,
		'SELECT task, COUNT(*) AS count, '
		'CAST(TOTAL(metadata.size) AS INTEGER) AS size '
		'FROM queue INNER JOIN metadata '
		'ON metadata.device = queue.device '
		'AND metadata.inode = queue.inode '
		'GROUP BY task',
		display_column=0,
		value_column=1,
	)


def query_redundancy_information() -> Data:
	columns: list[Column] = [
		Column('Similarity', lambda distance: 'Identical' if distance < 0 else '{:.1f}%'.format((64 - distance) / 0.64), lower=True),
		Column('Count'),
		Column('Size', format_size, tooltip=True),
	]

	return query_data(
		columns,
		'SELECT -1 AS distance, COUNT(*) AS count, '
		'CAST(TOTAL(size) AS INTEGER) AS size '
		'FROM metadata WHERE count_duplicate > 0 UNION '
		'SELECT distance, COUNT(*) AS count, '
		'CAST(TOTAL(metadata.size) AS INTEGER) AS size '
		'FROM similar INNER JOIN metadata '
		'ON metadata.device = similar.device_source '
		'AND metadata.inode = similar.inode_source '
		'GROUP BY distance',
		display_column=0,
		value_column=1,
	)


def query_video_codec_information(type: str) -> Data:
	columns: list[Column] = [Column('Codec'), Column('Count')]

	return query_data(
		columns,
		'SELECT video_codec, COUNT(*) AS count FROM metadata WHERE type = ? AND video_codec IS NOT NULL GROUP BY video_codec ORDER BY count DESC',
		type,
		display_column=0,
		value_column=1,
		row_limit=10,
	)


def query_audio_codec_information() -> Data:
	columns: list[Column] = [Column('Codec'), Column('Count')]

	return query_data(
		columns,
		'SELECT audio_codec, COUNT(*) AS count FROM metadata WHERE audio_codec IS NOT NULL GROUP BY audio_codec ORDER BY count DESC',
		display_column=0,
		value_column=1,
		row_limit=10,
	)


def query_resolution_information(type: str) -> Data:
	columns: list[Column] = [Column('Resolution'), Column('Count')]

	return query_data(
		columns,
		"SELECT width || 'x' || height AS resolution, COUNT(*) AS count "
		'FROM metadata WHERE type = ? '
		'AND width IS NOT NULL '
		'AND height IS NOT NULL '
		'GROUP BY width, height '
		'ORDER BY count DESC',
		type,
		display_column=0,
		value_column=1,
		row_limit=10,
	)
