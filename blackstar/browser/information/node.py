import contextlib
import dataclasses
import os
import pathlib
import typing

import bottle

from ...common.node import Node
from ..application import master
from ..database import get_media_db
from ..gallery import Entry, Fragment, Kind
from ..reference import get_or_create_reference_oplock
from ..template import fix_encoding
from .utility import ResolvedNode, get_limiter, resolve_node


@dataclasses.dataclass(frozen=True)
class Other(ResolvedNode):
	distance: int


@master.route('/node/<device:int>/<inode:int>', name='node', media=False, reference=True)
def information_node(device: int, inode: int) -> typing.Union[bottle.HTTPError, str]:
	if not bottle.request.token.can_info:
		return bottle.HTTPError(403, 'Information Restricted')

	bottle.response.set_header('Cache-Control', 'private, no-store')

	limiter = get_limiter()
	media_db = get_media_db()

	row = media_db.execute('SELECT * FROM metadata WHERE device = ? AND inode = ?', (device, inode)).fetchone()
	if row is None:
		return bottle.HTTPError(404, 'Node Not Found')

	node = Node(device, inode)
	resolved = resolve_node(limiter, node)

	data: dict[str, typing.Any] = dict(row)
	data.update(
		{
			'kind': kind_from_type(data['type']),
			'link': False,
			'reference': None,
			'path': pathlib.Path(os.devnull),
			'hidden': False,
			'display': resolved.display,
		}
	)

	del data['type']
	del data['perceptual']

	entry = Entry(**data)

	with contextlib.ExitStack() as stack:
		links = [decompose_oplock(stack, limiter.root, path) for path in resolved.paths]

	others: list[Other] = []

	if entry.hash_sha256:
		for row in media_db.execute(
			'SELECT device, inode FROM metadata WHERE hash_sha256 = ? AND NOT (device = ? AND inode = ?)',
			(entry.hash_sha256, node.device, node.inode),
		):
			other_node = Node(row['device'], row['inode'])
			other_resolved = resolve_node(limiter, other_node)
			others.append(Other(other_resolved.node, other_resolved.display, other_resolved.names, other_resolved.paths, -1))

	for row in media_db.execute(
		'SELECT device_target, inode_target, distance FROM similar WHERE device_source = ? AND inode_source = ? ORDER BY distance',
		(node.device, node.inode),
	):
		other_node = Node(row['device_target'], row['inode_target'])
		other_resolved = resolve_node(limiter, other_node)
		others.append(Other(other_resolved.node, other_resolved.display, other_resolved.names, other_resolved.paths, row['distance']))

	return bottle.template('information/node', entry=entry, links=links, others=others)


def kind_from_type(type: str) -> Kind:
	if type == 'D':
		return Kind.DIRECTORY
	elif type == 'F':
		return Kind.FILE
	elif type == 'I':
		return Kind.IMAGE
	elif type == 'A':
		return Kind.AUDIO
	elif type == 'V':
		return Kind.VIDEO
	else:
		return Kind.OTHER


def decompose_oplock(stack: contextlib.ExitStack, root: pathlib.PurePosixPath, path: pathlib.PurePosixPath) -> list[Fragment]:
	fragments: list[Fragment] = []
	while path != root:
		reference = get_or_create_reference_oplock(stack, None, path, False)
		fragments.insert(0, Fragment(reference, fix_encoding(path.name)))
		path = path.parent

	reference = get_or_create_reference_oplock(stack, None, root, False)
	fragments.insert(0, Fragment(reference))

	return fragments
