import typing

import bottle

from ..common.reference import ID_LENGTH
from .application import master
from .authorization import TokenSource, update_environ
from .download import download
from .gallery import gallery
from .reference import resolve_validate


@master.route('/<id:re:[0-9A-Za-z]{{{}}}>'.format(ID_LENGTH), name='dispatch', anonymous=True, reference=True, media=False)
def dispatch(id: str) -> typing.Union[str, bottle.HTTPResponse]:
	resolved = resolve_validate(id)

	if resolved.reference.token is not None:
		update_environ(resolved.reference.token)
		del bottle.request.token
		bottle.request.token = resolved.reference.token
		del bottle.request.token_source
		bottle.request.token_source = TokenSource.DATABASE

	return resolved.dispatch(file=download, directory=gallery)
