import logging
import traceback
import typing

import bottle
import gunicorn.config
import gunicorn.glogging

from ..common.logging import DATE_FORMAT, setup_logging
from ..common.logging import MESSAGE_FORMAT_PROCESS as MESSAGE_FORMAT

LOGGER = logging.getLogger('blackstar.browser')


class Logger(gunicorn.glogging.Logger):  # type: ignore[misc]
	hooks: list[typing.Callable[[gunicorn.config.Config], None]] = []

	error_fmt = MESSAGE_FORMAT
	datefmt = DATE_FORMAT

	def setup(self, cfg: gunicorn.config.Config) -> None:
		super().setup(cfg)

		logfile = cfg.env.pop('BLACKSTAR_LOGFILE', None)
		setup_logging(logfile, self.loglevel, self.error_fmt, self.datefmt, False)

		for hook in self.hooks:
			hook(cfg)


T = typing.TypeVar('T')
P = typing.ParamSpec('P')


class ExceptionPlugin:
	name = 'exception'
	api = 2

	def apply(self, callback: typing.Callable[P, T], route: bottle.Route) -> typing.Callable[P, typing.Union[T, bottle.HTTPError]]:
		def wrapper(*args: P.args, **kwargs: P.kwargs) -> typing.Union[T, bottle.HTTPError]:
			try:
				return callback(*args, **kwargs)
			except bottle.BottleException:
				raise
			except Exception as e:
				LOGGER.error('An exception occured processing request:', exc_info=e)
				return bottle.HTTPError(500, 'Internal Server Error', e, traceback.format_exc())

		return wrapper
