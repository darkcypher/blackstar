"use strict";

const intro_canvas = document.createElement("canvas");
const intro_particle = [];
const intro_vertex = 64;
const intro_edge = 6;

function random_position() { return Math.random(); }
function random_angle() { return Math.random() * Math.PI * 2.0; }
function random_speed() { return Math.random() * 0.05 + 0.05; }
function random_intensity() { return Math.random() * 0.5 + 0.5; }
function random_lifetime() { return Math.random() * 3.0 + 2.0; }

function colorize(intensity)
{
	const reference_r = 0.625;
	const reference_g = 0.25;
	const reference_b = 1.0;

	const r = (reference_r * intensity * 255.0) | 0;
	const g = (reference_g * intensity * 255.0) | 0;
	const b = (reference_b * intensity * 255.0) | 0;

	return `rgb(${r}, ${g}, ${b})`;
}

function intro_initialize()
{
	if (intro_particle.length)
		return;

	for (var i = 0; i < intro_vertex; ++i)
	{
		const x = random_position();
		const y = random_position();
		const angle = random_angle();
		const speed = random_speed();
		const intensity = random_intensity();
		const lifetime = random_lifetime();

		intro_particle.push({
			position: {
				logical: { x: x, y: y },
				physical: { x: null, y: null }
			},
			velocity: {
				x: Math.cos(angle) * speed,
				y: Math.sin(angle) * speed
			},
			intensity: intensity,
			lifetime: lifetime,
			update: 0.0,
			neighbor: null
		});
	}

	intro_particle.sort(function(left, right) { return left.intensity - right.intensity; });
}

function intro_update(time)
{
	const width = intro_canvas.width;
	const height = intro_canvas.height;

	const size = Math.min(width, height);
	const offset_x = width > height ? (width - size) / 2.0 : 0.0;
	const offset_y = height > width ? (height - size) / 2.0 : 0.0;

	for (const particle of intro_particle)
	{
		const dtime = time - particle.update;
		const position = particle.position;
		const logical = position.logical;
		const physical = position.physical;
		const velocity = particle.velocity;

		logical.x += velocity.x * dtime;
		if (logical.x < 0.0)
		{
			logical.x = -logical.x;
			velocity.x = -velocity.x;
		}
		else if (logical.x >= 1.0)
		{
			logical.x = 1.0 - (logical.x - 1.0);
			velocity.x = -velocity.x;
		}

		logical.y += velocity.y * dtime;
		if (logical.y < 0.0)
		{
			logical.y = -logical.y;
			velocity.y = -velocity.y;
		}
		else if (logical.y >= 1.0)
		{
			logical.y = 1.0 - (logical.y - 1.0);
			velocity.y = -velocity.y;
		}

		physical.x = (logical.x * size + offset_x) | 0;
		physical.y = (logical.y * size + offset_y) | 0;

		particle.lifetime -= dtime;
		if (particle.lifetime <= 0.0)
		{
			const angle = random_angle();
			const speed = random_speed();
			velocity.x = Math.cos(angle) * speed;
			velocity.y = Math.sin(angle) * speed;
			particle.lifetime = random_lifetime();
		}

		particle.update = time;
	}

	const count = intro_particle.length;
	for (var i = 0; i < count; ++i)
	{
		const p1 = intro_particle[i];
		const l1 = p1.position.logical;
		const x1 = l1.x;
		const y1 = l1.y;
		const n = [];

		for (var j = i + 1; j < count; ++j)
		{
			const p2 = intro_particle[j];
			const l2 = p2.position.logical;
			const x2 = l2.x;
			const y2 = l2.y;

			const dx = x1 - x2;
			const dy = y1 - y2;

			const distance = dx * dx + dy * dy;
			if (distance >= 0.0625)
				continue;

			n.push({ index: j, distance: distance });
		}

		n.sort(function (left, right) { return left.distance - right.distance; });
		p1.neighbor = n;
	}
}

function intro_render()
{
	const width = intro_canvas.width;
	const height = intro_canvas.height;

	const context = intro_canvas.getContext('2d');
	context.clearRect(0, 0, width, height);

	const lines = []
	for (const particle of intro_particle)
	{
		const l1 = particle.position.physical;
		const x1 = l1.x;
		const y1 = l1.y;
		const i1 = particle.intensity;

		for (const neighbor of particle.neighbor.slice(0, intro_edge))
		{
			const particle_other = intro_particle[neighbor.index];
			const l2 = particle_other.position.physical;
			const x2 = l2.x;
			const y2 = l2.y;
			const i2 = particle_other.intensity;

			const average_intensity = i1 + (i2 - i1) / 2.0;
			const reverse_distance = 1.0 - Math.min(Math.sqrt(neighbor.distance) * 4.0, 1.0);

			lines.push({
				x1: x1,
				y1: y1,
				x2: x2,
				y2: y2,
				intensity: average_intensity * reverse_distance
			});
		}
	}

	lines.sort(function(left, right) { return left.intensity - right.intensity; });

	context.lineWidth = 2;
	context.shadowBlur = 0;
	for (var line of lines)
	{
		const color = colorize(line.intensity);
		context.strokeStyle = color;
		context.shadowColor = color;
		context.beginPath();
		context.moveTo(line.x1, line.y1);
		context.lineTo(line.x2, line.y2);
		context.stroke();
	}

	context.shadowBlur = 4;
	for (const particle of intro_particle)
	{
		const physical = particle.position.physical;
		const color = colorize(particle.intensity)
		context.fillStyle = color;
		context.shadowColor = color;
		context.beginPath();
		context.ellipse(physical.x, physical.y, 6, 6, 0, 0.0, Math.PI * 2.0);
		context.fill();
	}
}

function intro_frame(time)
{
	const width = intro_canvas.clientWidth;
	const height = intro_canvas.clientHeight;

	if (intro_canvas.width != width)
		intro_canvas.width = width;
	if (intro_canvas.height != height)
		intro_canvas.height = height;

	intro_update(time / 1000.0);
	intro_render();

	window.requestAnimationFrame(intro_frame);
}

if (intro_canvas.getContext)
{
	intro_canvas.classList.add("intro");
	document.body.insertBefore(intro_canvas, document.body.firstChild);

	const container = document.querySelector("body.center > div.container");
	if (container)
		container.classList.add("intro");

	intro_initialize();
	window.requestAnimationFrame(intro_frame);
}
