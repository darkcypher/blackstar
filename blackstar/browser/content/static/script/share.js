function share_open(event)
{
	const button = event.currentTarget;
	const dialog = document.querySelector("dialog#share");

	dialog.dataset.id = button.dataset.shareId;

	const textbox = dialog.querySelector("input#share_url")
	textbox.value = '';
}

function share_start(event)
{
	const data = new FormData();
	const dialog = document.querySelector("dialog#share");

	data.append("id", dialog.dataset.id);
	data.append("expiration", parseInt(dialog.querySelector("input#share_expiration").value) * parseInt(dialog.querySelector("select#share_expiration_multiplier").value));
	data.append("download", dialog.querySelector("select#share_download").value);

	dialog.querySelector("button#share_start").classList.add("hidden")
	dialog.querySelector("span#share_spinner").classList.remove("hidden");

	const request = new XMLHttpRequest();
	request.onload = share_end;
	request.onerror = share_end;
	request.ontimeout = share_end;
	request.open("POST", dialog.dataset.endpoint);

	request.send(data);

	event.preventDefault();
}

function share_end(event)
{
	const location = this.getResponseHeader("Location");
	const dialog = document.querySelector("dialog#share");
	const textbox = dialog.querySelector("input#share_url")

	if (location)
	{
		textbox.value = location;
		textbox.select();
	}
	else
	{
		textbox.value = '';
	}

	dialog.querySelector("span#share_spinner").classList.add("hidden");
	dialog.querySelector("button#share_start").classList.remove("hidden")

	if (!location)
		alert("share error");

	event.preventDefault();
}

function share_copy(event)
{
	const textbox = document.querySelector("input#share_url");
	textbox.select();
	document.execCommand('copy');
	event.preventDefault();
}

for (const button of document.querySelectorAll("button[data-dialog-show=\"share\"][data-share-id]"))
	button.addEventListener("click", share_open)

document.querySelector("button#share_start").addEventListener("click", share_start);
document.querySelector("button#share_copy").addEventListener("click", share_copy);
