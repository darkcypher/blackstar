"use strict";

function button_copy(event)
{
	const button = event.currentTarget;

	const target_start = button.dataset.copyStart;
	const element_start = document.querySelector(target_start);
	if (!element_start)
		return;

	const target_end = button.dataset.copyEnd;
	const element_end = document.querySelector(target_end);
	if (!element_end)
		return;

	const selection = window.getSelection();
	const range = document.createRange();

	range.setStartBefore(element_start);
	range.setEndAfter(element_end);

	selection.removeAllRanges();
	selection.addRange(range);
	document.execCommand('copy');
	selection.removeAllRanges();

	event.preventDefault();
}

function button_navigate(event)
{
	const button = event.currentTarget;
	const uri = button.dataset.uri;
	window.location.assign(uri);
	event.preventDefault();
}

function button_setcookie(event)
{
	const button = event.currentTarget;
	const cookie = button.dataset.cookie;
	document.cookie = cookie;
	window.location.reload();
	event.preventDefault();
}

function button_dialog_show(event)
{
	const button = event.currentTarget;
	const dialog = button.dataset.dialogShow;
	document.querySelector("dialog#" + dialog).showModal();
	event.preventDefault();
}

function button_dialog_close(event)
{
	const button = event.currentTarget;
	const dialog = button.dataset.dialogClose;
	document.querySelector("dialog#" + dialog).close();
	event.preventDefault();
}

function hookup(selector, listener, type="click")
{
	for (const button of document.querySelectorAll(selector))
		button.addEventListener(type, listener);
}

hookup("button[data-copy-start][data-copy-end]", button_copy);
hookup("button[data-uri]", button_navigate);
hookup("button[data-cookie]", button_setcookie);
hookup("button[data-dialog-show]", button_dialog_show);
hookup("button[data-dialog-close]", button_dialog_close);
