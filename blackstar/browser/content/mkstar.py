import math

BLACK = '#000000'
WHITE = '#ffffff'
ACCENT = '#a040ff'
SIZE = 256


def color_to_tuple(input: str) -> tuple[float, float, float]:
	return (int(input[1:3], 16) / 255, int(input[3:5], 16) / 255, int(input[5:7], 16) / 255)


def tuple_to_color(input: tuple[float, float, float]) -> str:
	return '#{:02x}{:02x}{:02x}'.format(int(input[0] * 255.0), int(input[1] * 255.0), int(input[2] * 255.0))


def mix_colors(color1: tuple[float, float, float], color2: tuple[float, float, float], offset: float) -> tuple[float, float, float]:
	def lerp(v1: float, v2: float, o: float) -> float:
		if v1 > v2:
			v2, v1 = v1, v2
			o = 1.0 - o
		return v1 + (v2 - v1) * o

	return (lerp(color1[0], color2[0], offset), lerp(color1[1], color2[1], offset), lerp(color1[2], color2[2], offset))


def star(radius: float, color: tuple[float, float, float]) -> None:
	halfsize = SIZE / 2.0
	radius *= halfsize
	print('<path d="', end='')
	for index in range(0, 5):
		theta = math.pi * (index / 1.25) - math.pi / 2.0
		x = round(math.cos(theta) * radius, 3)
		y = round(math.sin(theta) * radius, 3)
		print('{} {},{} '.format('L' if index else 'M', x, y), end='')
	print('Z" fill="{}" />'.format(tuple_to_color(color)))


def execute() -> None:
	print('<?xml version="1.0" encoding="UTF-8" standalone="no"?>')

	xy = int(SIZE / -2)
	wh = SIZE

	print('<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="{0} {0} {1} {1}">'.format(xy, wh))
	print('<mask id="shape">')
	print('<circle x="0" y="0" r="{}" fill="white" />'.format(int(SIZE / 2.0)))
	print('</mask>')
	print('<g mask="url(#shape)">')
	print('<rect x="{0}" y="{0}" width="{1}" height="{1}" fill="black" />'.format(xy, wh))

	black = color_to_tuple(BLACK)
	white = color_to_tuple(WHITE)
	accent = color_to_tuple(ACCENT)

	star(63.0 / 32.0, mix_colors(black, accent, 1.0 / 4.0))
	star(56.0 / 32.0, mix_colors(black, accent, 2.0 / 4.0))
	star(49.0 / 32.0, mix_colors(black, accent, 3.0 / 4.0))
	star(42.0 / 32.0, mix_colors(black, accent, 4.0 / 4.0))
	star(35.0 / 32.0, white)
	star(24.5 / 32.0, black)

	print('</g>')
	print('</svg>')
	pass


if __name__ == '__main__':
	execute()
