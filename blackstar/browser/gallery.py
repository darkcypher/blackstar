import contextlib
import dataclasses
import datetime
import enum
import functools
import json
import os
import pathlib
import shlex
import stat
import sys
import typing
import urllib.parse
import xml.etree.ElementTree as ET

import bottle
import markdown
import markdown.treeprocessors

from .configuration import get_current as get_configuration
from .configuration import get_mapper
from .database import get_media_db
from .download import make_content_disposition
from .reference import Reference, ReferenceCreator, ResolvedReference, get_or_create_reference_oplock, make_pure_path, update_environ
from .template import fix_encoding, format_size, format_time
from .thumbnail import get_thumbnail_url

ReferenceFactory = typing.Callable[[pathlib.Path], Reference]


@dataclasses.dataclass(frozen=True)
class Fragment:
	reference: Reference
	display: str = ''


class Kind(enum.Enum):
	DIRECTORY = enum.auto()
	FILE = enum.auto()
	IMAGE = enum.auto()
	AUDIO = enum.auto()
	VIDEO = enum.auto()
	OTHER = enum.auto()


@dataclasses.dataclass(frozen=True)
class Entry:
	kind: Kind
	link: bool
	reference: typing.Optional[Reference]
	path: pathlib.Path
	hidden: bool
	display: str
	device: typing.Optional[int] = None
	inode: typing.Optional[int] = None
	size: typing.Optional[int] = None
	atime: typing.Optional[float] = None
	mtime: typing.Optional[float] = None
	ctime: typing.Optional[float] = None
	comment: typing.Optional[str] = None
	weight: typing.Optional[int] = None
	hash_md5: typing.Optional[bytes] = None
	hash_sha1: typing.Optional[bytes] = None
	hash_sha256: typing.Optional[bytes] = None
	width: typing.Optional[int] = None
	height: typing.Optional[int] = None
	duration: typing.Optional[float] = None
	frame_rate: typing.Optional[float] = None
	video_codec: typing.Optional[str] = None
	video_rate: typing.Optional[int] = None
	audio_codec: typing.Optional[str] = None
	audio_rate: typing.Optional[int] = None
	exif_make: typing.Optional[str] = None
	exif_model: typing.Optional[str] = None
	exif_software: typing.Optional[str] = None
	exif_latitude: typing.Optional[float] = None
	exif_longitude: typing.Optional[float] = None
	thumbnail_1x: typing.Optional[str] = None
	thumbnail_2x: typing.Optional[str] = None
	thumbnail_3x: typing.Optional[str] = None
	thumbnail_4x: typing.Optional[str] = None
	preview_1x: typing.Optional[str] = None
	preview_2x: typing.Optional[str] = None
	count_item: typing.Optional[int] = None
	count_link: typing.Optional[int] = None
	count_similar: typing.Optional[int] = None
	count_duplicate: typing.Optional[int] = None


class Sort(enum.Enum):
	NAME = enum.auto()
	SIZE = enum.auto()
	TIME = enum.auto()
	DURATION = enum.auto()


class Order(enum.Enum):
	ASCENDING = enum.auto()
	DESCENDING = enum.auto()


class View(enum.Enum):
	LIST = enum.auto()
	GRID = enum.auto()
	LARGE = enum.auto()
	TEXT = enum.auto()
	JSON = enum.auto()
	PLAYLIST = enum.auto()
	WGET = enum.auto()
	CURL = enum.auto()
	MD5 = enum.auto()
	SHA1 = enum.auto()
	SHA256 = enum.auto()
	BBCODE = enum.auto()


class Format(enum.Enum):
	MARKDOWN = enum.auto()
	PLAIN = enum.auto()


@dataclasses.dataclass(frozen=True)
class Parameters:
	fragments: list[Fragment]
	entries: list[Entry]
	view: View
	sort: Sort
	order: Order
	mixed: bool
	readme: typing.Optional[str]


@dataclasses.dataclass(frozen=True)
class Handler:
	handler: typing.Callable[[Parameters], str]
	content_type: typing.Optional[str]
	extension: str
	readme: bool = False


HANDLERS: dict[View, Handler] = {}

Sorter = typing.Callable[[Entry], tuple[object, ...]]


def gallery(resolved: ResolvedReference) -> typing.Union[str, bottle.HTTPError]:
	if not os.access(resolved.target, os.R_OK | os.X_OK, follow_symlinks=False):
		return bottle.HTTPError(403, 'Target Path Forbidden')
	if not bottle.request.token.can_browse:
		return bottle.HTTPError(403, 'Browse Restricted')

	update_environ('browse', resolved)
	bottle.response.set_header('Cache-Control', 'private, no-store' if resolved.reference.token is None else 'no-store')

	with contextlib.ExitStack() as stack:
		factory = make_reference_factory(resolved.reference, functools.partial(get_or_create_reference_oplock, stack))
		fragments = decompose(factory, resolved.root, resolved.target)
		entries = scan(factory, resolved.root, resolved.target)

	view = get_view_mode()
	sort, order, mixed = get_sort_info()
	entries.sort(key=get_sorter(sort, mixed), reverse=order == Order.DESCENDING)

	handler = HANDLERS[view]

	readme: typing.Optional[str] = None
	if handler.readme:
		path, format = get_readme(resolved.root, resolved.target)
		if path is not None:
			if format == Format.MARKDOWN:
				with contextlib.ExitStack() as stack:
					factory = make_reference_factory(resolved.reference, functools.partial(get_or_create_reference_oplock, stack))
					readme = parse_readme_markdown(factory, resolved.root, resolved.target, path)
			elif format == Format.PLAIN:
				readme = parse_readme_text(path)

	if handler.content_type:
		bottle.response.content_type = handler.content_type
	bottle.response.set_header('Content-Disposition', make_content_disposition('inline', fragments[-1].display or '-') + handler.extension)
	parameters = Parameters(fragments, entries, view, sort, order, mixed, readme)
	result = handler.handler(parameters)

	return result


def view_html(parameters: Parameters) -> str:
	return typing.cast(
		str,
		bottle.template(
			'gallery/{}'.format(parameters.view.name.lower()),
			fragments=parameters.fragments,
			entries=parameters.entries,
			view=parameters.view,
			sort=parameters.sort,
			order=parameters.order,
			mixed=parameters.mixed,
			readme=parameters.readme,
		),
	)


HANDLERS[View.LIST] = Handler(view_html, None, '.html', True)
HANDLERS[View.GRID] = HANDLERS[View.LIST]
HANDLERS[View.LARGE] = HANDLERS[View.LIST]


def view_text(parameters: Parameters) -> str:
	return '\n'.join(href for href in {reference_to_href(entry.reference) for entry in parameters.entries} if href is not None)


HANDLERS[View.TEXT] = Handler(view_text, 'text/plain; charset=UTF-8', '.txt')


def view_json(parameters: Parameters) -> str:
	return json.dumps(list(map(entry_to_json, parameters.entries)))


HANDLERS[View.JSON] = Handler(view_json, 'application/json; charset=UTF-8', '.json')


def view_playlist(parameters: Parameters) -> str:
	result = ['#EXTM3U']
	for entry in parameters.entries:
		if entry.kind not in (Kind.AUDIO, Kind.VIDEO) or entry.reference is None:
			continue

		result.extend(['#EXTINF:{},{}'.format(int(entry.duration or 0.0), entry.display), typing.cast(str, reference_to_href(entry.reference))])

	return '\n'.join(result)


HANDLERS[View.PLAYLIST] = Handler(view_playlist, 'application/vnd.apple.mpegurl; charset=UTF-8', '.m3u8')


def view_wget(parameters: Parameters) -> str:
	result = ['#!/bin/sh']
	for entry in parameters.entries:
		if entry.kind == Kind.DIRECTORY or entry.reference is None:
			continue

		result.append(' '.join(map(shlex.quote, ['wget', '-c', '-O', entry.display, typing.cast(str, reference_to_href(entry.reference))])))

	return '\n'.join(result)


HANDLERS[View.WGET] = Handler(view_wget, 'application/x-sh; charset=UTF-8', '.sh')


def view_curl(parameters: Parameters) -> str:
	result = ['#!/bin/sh']
	for entry in parameters.entries:
		if entry.kind == Kind.DIRECTORY or entry.reference is None:
			continue

		result.append(
			' '.join(map(shlex.quote, ['curl', '-C', '-', '-f', '-o', entry.display, typing.cast(str, reference_to_href(entry.reference))]))
		)

	return '\n'.join(result)


HANDLERS[View.CURL] = Handler(view_curl, 'application/x-sh; charset=UTF-8', '.sh')


def view_md5(parameters: Parameters) -> str:
	result: list[str] = []
	for entry in parameters.entries:
		if entry.kind == Kind.DIRECTORY or entry.hash_md5 is None:
			continue

		result.append('{}  {}'.format(entry.hash_md5.hex(), entry.display))

	return '\n'.join(result)


HANDLERS[View.MD5] = Handler(view_md5, 'text/plain; charset=UTF-8', '.md5')


def view_sha1(parameters: Parameters) -> str:
	result: list[str] = []
	for entry in parameters.entries:
		if entry.kind == Kind.DIRECTORY or entry.hash_sha1 is None:
			continue

		result.append('{}  {}'.format(entry.hash_sha1.hex(), entry.display))

	return '\n'.join(result)


HANDLERS[View.SHA1] = Handler(view_sha1, 'text/plain; charset=UTF-8', '.sha1')


def view_sha256(parameters: Parameters) -> str:
	result: list[str] = []
	for entry in parameters.entries:
		if entry.kind == Kind.DIRECTORY or entry.hash_sha256 is None:
			continue

		result.append('{}  {}'.format(entry.hash_sha256.hex(), entry.display))

	return '\n'.join(result)


HANDLERS[View.SHA256] = Handler(view_sha256, 'text/plain; charset=UTF-8', '.sha256')


def view_bbcode(parameters: Parameters) -> str:
	return '\n'.join(
		'[url={}]{}[/url]'.format(reference_to_href(entry.reference), entry.display)
		for entry in parameters.entries
		if entry.kind != Kind.DIRECTORY and entry.reference is not None
	)


HANDLERS[View.BBCODE] = Handler(view_bbcode, 'text/plain; charset=UTF-8', '.txt')


def get_parameter(name: str) -> typing.Optional[str]:
	value: typing.Optional[str] = bottle.request.query.get(name)
	if value is None:
		value = bottle.request.cookies.get(name)
	return value


TEnum = typing.TypeVar('TEnum', bound=enum.Enum)


def get_parameter_enum(name: str, type: typing.Type[TEnum], fallback: TEnum) -> TEnum:
	value = get_parameter(name)
	if value is None:
		return fallback
	evalue: typing.Optional[TEnum] = type.__members__.get(value.upper())
	if evalue is None:
		return fallback
	return evalue


def get_parameter_bool(name: str, fallback: bool) -> bool:
	value = get_parameter(name)
	if value is None:
		return fallback
	try:
		return bool(int(value))
	except ValueError:
		return fallback


def get_view_mode() -> View:
	return get_parameter_enum('view', View, View.LARGE)


def get_sort_info() -> tuple[Sort, Order, bool]:
	return (get_parameter_enum('sort', Sort, Sort.NAME), get_parameter_enum('order', Order, Order.ASCENDING), get_parameter_bool('mixed', False))


def sorter_name(entry: Entry) -> tuple[object, ...]:
	return (entry.display.lower(),)


def sorter_size(entry: Entry, discriminator: Sorter = sorter_name) -> tuple[object, ...]:
	return (entry.weight or entry.size or 0, *discriminator(entry))


def sorter_time(entry: Entry, discriminator: Sorter = sorter_name) -> tuple[object, ...]:
	return (entry.mtime or 0.0, *discriminator(entry))


def sorter_duration(entry: Entry, discriminator: Sorter = sorter_name) -> tuple[object, ...]:
	return (entry.duration or 0.0, *discriminator(entry))


def sorter_demixer(entry: Entry, discriminator: Sorter = sorter_name) -> tuple[object, ...]:
	return (0 if entry.kind == Kind.DIRECTORY else 1, *discriminator(entry))


def get_sorter(sort: Sort, mixed: bool) -> Sorter:
	sorter: Sorter = sorter_name
	if sort == Sort.SIZE:
		sorter = functools.partial(sorter_size, discriminator=sorter)
	elif sort == Sort.TIME:
		sorter = functools.partial(sorter_time, discriminator=sorter)
	elif sort == Sort.DURATION:
		sorter = functools.partial(sorter_duration, discriminator=sorter)
	if not mixed:
		sorter = functools.partial(sorter_demixer, discriminator=sorter)
	return sorter


def is_hidden(display: str, extra: typing.Optional[frozenset[str]] = None) -> bool:
	if display.startswith('.'):
		return True
	elif extra is None:
		return False
	else:
		return display in extra


def get_small_icon(kind: Kind) -> str:
	if kind == Kind.DIRECTORY:
		return 'folder-open'
	elif kind == Kind.FILE:
		return 'file'
	elif kind == Kind.IMAGE:
		return 'image-o'
	elif kind == Kind.AUDIO:
		return 'audio-o'
	elif kind == Kind.VIDEO:
		return 'video-o'
	else:
		return 'file-o'


def get_large_icon(kind: Kind) -> str:
	if kind == Kind.DIRECTORY:
		return 'folder-open-o'
	elif kind == Kind.IMAGE:
		return 'picture-o'
	elif kind == Kind.AUDIO:
		return 'music'
	elif kind == Kind.VIDEO:
		return 'film'
	else:
		return 'file-o'


def get_info(entry: Entry) -> str:
	duration = int(entry.duration or 0.0)
	if entry.kind in (Kind.AUDIO, Kind.VIDEO) and duration:
		return str(datetime.timedelta(seconds=duration))

	if entry.width and entry.height:
		return '{}x{}'.format(entry.width, entry.height)

	return '-'


def get_summary(entry: Entry) -> str:
	lines: list[str] = []
	segments: list[str] = []

	if entry.comment:
		lines.extend(entry.comment.splitlines())

	if entry.width and entry.height:
		segments.append('{}x{}'.format(entry.width, entry.height))

	duration = int(entry.duration or 0.0)
	if duration:
		segments.append(str(datetime.timedelta(seconds=duration)))

	if entry.frame_rate:
		segments.append('{:.2f}'.format(round(entry.frame_rate, 2)))

	lines.append(' - '.join(segments))
	segments.clear()

	if entry.video_codec:
		if entry.video_rate:
			segments.append('{} {}k'.format(entry.video_codec, entry.video_rate // 1024))
		else:
			segments.append(entry.video_codec)
	if entry.audio_codec:
		if entry.audio_rate:
			segments.append('{} {}k'.format(entry.audio_codec, entry.audio_rate // 1024))
		else:
			segments.append(entry.audio_codec)

	lines.append(' / '.join(segments))
	segments.clear()

	if entry.weight:
		lines.append(format_size(entry.weight))
	elif entry.size:
		lines.append(format_size(entry.size))
	if entry.mtime:
		lines.append(format_time(entry.mtime))

	return '\n'.join([line for line in lines if line][:3])


def make_reference_factory(reference: Reference, creator: ReferenceCreator) -> ReferenceFactory:
	def factory(path: pathlib.Path) -> Reference:
		path_pure = make_pure_path(path)
		return creator(reference.token, path_pure, reference.download)

	return factory


def decompose(factory: ReferenceFactory, root: pathlib.Path, target: pathlib.Path) -> list[Fragment]:
	fragments: list[Fragment] = []
	while target != root:
		fragments.insert(0, Fragment(factory(target), fix_encoding(target.name)))
		target = target.parent

	fragments.insert(0, Fragment(factory(target)))

	return fragments


def scan(factory: ReferenceFactory, root: pathlib.Path, target: pathlib.Path) -> list[Entry]:
	entries: list[Entry] = []

	if sys.platform == 'win32':

		def make_st_factory(entry: os.DirEntry[str]) -> typing.Callable[[], os.stat_result]:
			return functools.partial(os.lstat, entry.path)
	else:

		def make_st_factory(entry: os.DirEntry[str]) -> typing.Callable[[], os.stat_result]:
			return functools.partial(entry.stat, follow_symlinks=False)

	for entry in os.scandir(target):
		new_path = pathlib.Path(entry.path)
		new_entry = make_entry(factory, root, new_path, fix_encoding(entry.name), make_st_factory(entry))
		if new_entry is None:
			continue
		entries.append(new_entry)

	return entries


def probe(
	root: pathlib.Path, path: pathlib.Path, st_factory: typing.Optional[typing.Callable[[], os.stat_result]] = None
) -> tuple[Kind, pathlib.Path, bool, typing.Optional[os.stat_result]]:
	if st_factory is None:
		st_factory = functools.partial(path.stat, follow_symlinks=False)

	try:
		st = st_factory()
	except Exception:
		return Kind.OTHER, path, False, None

	if stat.S_ISDIR(st.st_mode):
		return Kind.DIRECTORY, path, False, st
	elif stat.S_ISREG(st.st_mode):
		return Kind.FILE, path, False, st
	elif not stat.S_ISLNK(st.st_mode):
		return Kind.OTHER, path, False, st

	try:
		path = path.resolve(True)
	except Exception:
		return Kind.OTHER, path, True, None

	try:
		st = path.lstat()
	except Exception:
		return Kind.OTHER, path, True, None

	if not path.is_relative_to(root):
		return Kind.OTHER, path, True, None

	if stat.S_ISDIR(st.st_mode):
		return Kind.DIRECTORY, path, True, st
	elif stat.S_ISREG(st.st_mode):
		return Kind.FILE, path, True, st
	else:
		return Kind.OTHER, path, True, st


def make_entry(
	factory: ReferenceFactory, root: pathlib.Path, path: pathlib.Path, display: str, st_factory: typing.Callable[[], os.stat_result]
) -> typing.Optional[Entry]:
	kind, path, link, st = probe(root, path, st_factory)

	if kind == Kind.DIRECTORY:
		assert st is not None
		return make_directory_entry(factory, link, path, display, st, get_linked_stat(root, path))
	elif kind == Kind.FILE:
		assert st is not None
		return make_file_entry(factory, link, path, display, st)
	elif kind == Kind.OTHER:
		return make_other_entry(factory, link, path, display, st)
	else:
		return None


def get_linked_stat(root: pathlib.Path, path: pathlib.Path) -> typing.Optional[os.stat_result]:
	cfg = get_configuration()
	kind, _, _, st = probe(root, path / cfg.extension.linked)
	return st if kind == Kind.FILE else None


def make_entry_kwargs_basic(
	kind: Kind, link: bool, reference: typing.Optional[Reference], path: pathlib.Path, hidden: bool, display: str
) -> dict[str, typing.Any]:
	return {'kind': kind, 'link': link, 'reference': reference, 'path': path, 'hidden': hidden, 'display': display}


def make_entry_kwargs_stat(st: typing.Optional[os.stat_result]) -> dict[str, typing.Any]:
	if not st:
		return {}

	result: dict[str, typing.Any] = {}

	if bottle.request.token.can_info:
		node = get_mapper().resolve(st)
		if node is not None:
			result.update({'device': node.device, 'inode': node.inode})

	if stat.S_ISREG(st.st_mode):
		result['size'] = st.st_size

	result.update({'atime': st.st_atime, 'mtime': st.st_mtime, 'ctime': st.st_ctime})

	return result


def make_entry_kwargs_xattr(st: typing.Optional[os.stat_result], path: pathlib.Path) -> dict[str, typing.Any]:
	if not st:
		return {}

	try:
		comment = os.getxattr(path, 'user.xdg.comment', follow_symlinks=False)
	except Exception:
		return {}

	return {'comment': comment.decode('utf-8', 'replace')}


def make_entry_kwargs_container(st: os.stat_result) -> dict[str, typing.Any]:
	node = get_mapper().resolve(st)
	if node is None:
		return {}

	if not bottle.request.token.can_info:
		return {}

	db = get_media_db()
	row = db.execute('SELECT weight, count_item FROM metadata WHERE device = ? AND inode = ? LIMIT 1', (node.device, node.inode)).fetchone()

	if row is None:
		return {}

	return dict(row)


def make_entry_kwargs_media(kind: Kind, st: os.stat_result) -> dict[str, typing.Any]:
	node = get_mapper().resolve(st)
	if node is None:
		return {}

	wanted: list[str] = []

	if kind != Kind.DIRECTORY:
		if bottle.request.token.can_download:
			wanted.extend(['hash_md5', 'hash_sha1', 'hash_sha256'])

		if kind == Kind.IMAGE:
			wanted.extend(['width', 'height', 'video_codec'])

		elif kind == Kind.AUDIO:
			wanted.extend(['duration', 'audio_codec', 'audio_rate'])

		elif kind == Kind.VIDEO:
			wanted.extend(['width', 'height', 'duration', 'frame_rate', 'video_codec', 'video_rate', 'audio_codec', 'audio_rate'])

		if kind in (Kind.IMAGE, Kind.VIDEO):
			if bottle.request.token.can_download or bottle.request.token.can_info:
				wanted.extend(['exif_make', 'exif_model', 'exif_software', 'exif_latitude', 'exif_longitude'])

	if kind in (Kind.DIRECTORY, Kind.IMAGE, Kind.VIDEO):
		wanted.extend(['thumbnail_1x', 'thumbnail_2x', 'thumbnail_3x', 'thumbnail_4x'])

	if kind == Kind.VIDEO:
		wanted.extend(['preview_1x', 'preview_2x'])

	if kind != Kind.DIRECTORY and bottle.request.token.can_info:
		wanted.extend(['count_link', 'count_similar', 'count_duplicate'])

	if not wanted:
		return {}

	db = get_media_db()
	stmt = 'SELECT {} FROM metadata WHERE device = ? AND inode = ? AND size = ? AND mtime = ? LIMIT 1'.format(', '.join(wanted))

	row = db.execute(stmt, (node.device, node.inode, st.st_size, st.st_mtime)).fetchone()

	if row is None:
		return {}

	return dict(row)


def make_directory_entry(
	factory: ReferenceFactory, link: bool, path: pathlib.Path, display: str, st: os.stat_result, linked_st: typing.Optional[os.stat_result]
) -> typing.Optional[Entry]:
	cfg = get_configuration()
	reference = factory(path) if bottle.request.token.can_browse else None

	hidden = is_hidden(display, cfg.hidden.directory)
	if hidden and not bottle.request.token.can_xray:
		return None

	kwargs = make_entry_kwargs_basic(Kind.DIRECTORY, link, reference, path, hidden, display)

	kwargs.update(make_entry_kwargs_stat(st))
	kwargs.update(make_entry_kwargs_xattr(st, path))

	kwargs.update(make_entry_kwargs_container(st))
	if linked_st is not None:
		kwargs.update(make_entry_kwargs_media(Kind.DIRECTORY, linked_st))

	return Entry(**kwargs)


def make_file_entry(factory: ReferenceFactory, link: bool, path: pathlib.Path, display: str, st: os.stat_result) -> typing.Optional[Entry]:
	cfg = get_configuration()

	extension = os.path.splitext(display)[1].lower()
	if extension in cfg.extension.image:
		kind = Kind.IMAGE
	elif extension in cfg.extension.audio:
		kind = Kind.AUDIO
	elif extension in cfg.extension.video:
		kind = Kind.VIDEO
	else:
		kind = Kind.FILE

	reference = factory(path) if bottle.request.token.can_download else None

	hidden = is_hidden(display, cfg.hidden.file)
	if hidden and not bottle.request.token.can_xray:
		return None

	if display == cfg.extension.linked and cfg.hidden.linked:
		return None

	kwargs = make_entry_kwargs_basic(kind, link, reference, path, hidden, display)

	kwargs.update(make_entry_kwargs_stat(st))
	kwargs.update(make_entry_kwargs_xattr(st, path))
	kwargs.update(make_entry_kwargs_media(kind, st))

	return Entry(**kwargs)


def make_other_entry(
	factory: ReferenceFactory, link: bool, path: pathlib.Path, display: str, st: typing.Optional[os.stat_result]
) -> typing.Optional[Entry]:
	hidden = is_hidden(display)
	if hidden and not bottle.request.token.can_xray:
		return None

	kwargs = make_entry_kwargs_basic(Kind.OTHER, link, None, path, hidden, display)

	kwargs.update(make_entry_kwargs_stat(st))
	kwargs.update(make_entry_kwargs_xattr(st, path))

	return Entry(**kwargs)


def get_readme(root: pathlib.Path, path: pathlib.Path) -> typing.Union[tuple[None, None], tuple[pathlib.Path, Format]]:
	cfg = get_configuration()

	for readme in cfg.readme.markdown:
		kind, readme_path, _, _ = probe(root, path / readme)
		if kind == Kind.FILE:
			return readme_path, Format.MARKDOWN

	for readme in cfg.readme.plain:
		kind, readme_path, _, _ = probe(root, path / readme)
		if kind == Kind.FILE:
			return readme_path, Format.PLAIN

	return None, None


class UrlRewriteTreeProcessor(markdown.treeprocessors.Treeprocessor):
	def __init__(self, md: markdown.Markdown, factory: ReferenceFactory, root: pathlib.Path, target: pathlib.Path) -> None:
		super().__init__(md)
		self.factory = factory
		self.root = root
		self.target = target

	def run(self, root: ET.Element) -> typing.Optional[ET.Element]:
		if isinstance(root, ET.Element):
			self.process(root)
		return None

	def process(self, element: ET.Element) -> None:
		for child in element:
			if isinstance(child, ET.Element):
				self.process(child)

		if element.tag == 'a':
			self.process_a(element)
		elif element.tag == 'img':
			self.process_img(element)

	def process_a(self, element: ET.Element) -> None:
		href = element.attrib.get('href')
		if href is None:
			return

		href, external = self.translate_href(href)

		if href is None:
			del element.attrib['href']
			return

		element.attrib['href'] = href

		if not external:
			return

		element.attrib['rel'] = 'noreferrer'

		clazz = element.attrib.get('class')
		if clazz is None:
			clazz = 'external'
		else:
			clazz += ' external'
		element.attrib['class'] = clazz

	def translate_href(self, url: str) -> typing.Union[tuple[str, bool], tuple[None, None]]:
		split = urllib.parse.urlsplit(url)
		if split.scheme or split.netloc:
			return url, True

		if not split.path and split.fragment:
			return url, False

		if split.path.startswith('/'):
			return None, None

		path = pathlib.Path(os.path.normpath(self.target / split.path))
		if not path.is_relative_to(self.root):
			return None, None

		kind, path, _, _ = probe(self.root, path)
		if kind not in (Kind.DIRECTORY, Kind.FILE):
			return None, None

		if kind == Kind.FILE and not bottle.request.token.can_download:
			return None, None

		reference = self.factory(path)
		return typing.cast(str, bottle.request.app.get_url('dispatch', id=reference.id)), False

	def process_img(self, element: ET.Element) -> None:
		src = element.attrib.get('src')
		if src is None:
			return

		src = self.translate_src(src)

		if src is None:
			del element.attrib['src']
		else:
			element.attrib['src'] = src

	def translate_src(self, url: str) -> typing.Optional[str]:
		split = urllib.parse.urlsplit(url)
		if split.scheme or split.netloc:
			return None

		if not split.path and split.fragment:
			return None

		if split.path.startswith('/'):
			return None

		path = pathlib.Path(os.path.normpath(self.target / split.path))
		if not path.is_relative_to(self.root):
			return None

		kind, path, _, _ = probe(self.root, path)
		if kind != Kind.FILE:
			return None

		if not bottle.request.token.can_download:
			return None

		reference = self.factory(path)
		return typing.cast(str, bottle.request.app.get_url('dispatch', id=reference.id))


def parse_readme_markdown(factory: ReferenceFactory, root: pathlib.Path, target: pathlib.Path, path: pathlib.Path) -> str:
	with path.open('r', encoding='utf-8', errors='replace') as fp:
		text = fp.read()

	md = markdown.Markdown(extensions=['extra', 'nl2br'], output_format='html')
	md.preprocessors.deregister('html_block')
	md.treeprocessors.register(UrlRewriteTreeProcessor(md, factory, root, target), 'url_rewrite', 0)
	md.inlinePatterns.deregister('html')

	return md.convert(text)


def parse_readme_text(path: pathlib.Path) -> str:
	with path.open('r', encoding='utf-8', errors='replace') as fp:
		text = fp.read()

	return '<pre>{}</pre>'.format(bottle.html_escape(text))


def reference_to_href(reference: typing.Optional[Reference]) -> typing.Optional[str]:
	if reference is None:
		return None

	return urllib.parse.urljoin(typing.cast(str, bottle.request.url), typing.cast(str, bottle.request.app.get_url('dispatch', id=reference.id)))


def hash_to_hex(hash: typing.Optional[bytes]) -> typing.Optional[str]:
	if hash is None:
		return None

	return hash.hex().upper()


def thumbnail_to_href(id: typing.Optional[str], animated: bool) -> typing.Optional[str]:
	if id is None:
		return None

	return urllib.parse.urljoin(typing.cast(str, bottle.request.url), get_thumbnail_url(id, animated))


def entry_to_json(entry: Entry) -> dict[str, object]:
	return remove_none(
		{
			'kind': entry.kind.name.lower(),
			'link': entry.link,
			'href': reference_to_href(entry.reference),
			'hidden': entry.hidden,
			'display': entry.display,
			'device': entry.device,
			'inode': entry.inode,
			'size': entry.size,
			'atime': entry.atime,
			'mtime': entry.mtime,
			'ctime': entry.ctime,
			'comment': entry.comment,
			'weight': entry.weight,
			'hash': {'md5': hash_to_hex(entry.hash_md5), 'sha1': hash_to_hex(entry.hash_sha1), 'sha256': hash_to_hex(entry.hash_sha256)},
			'width': entry.width,
			'height': entry.height,
			'duration': entry.duration,
			'frame_rate': entry.frame_rate,
			'video': {'codec': entry.video_codec, 'rate': entry.video_rate},
			'audio': {'codec': entry.audio_codec, 'rate': entry.audio_rate},
			'exif': {
				'make': entry.exif_make,
				'model': entry.exif_model,
				'software': entry.exif_software,
				'latitude': entry.exif_latitude,
				'longitude': entry.exif_longitude,
			},
			'thumbnail': {
				'1x': thumbnail_to_href(entry.thumbnail_1x, False),
				'2x': thumbnail_to_href(entry.thumbnail_2x, False),
				'3x': thumbnail_to_href(entry.thumbnail_3x, False),
				'4x': thumbnail_to_href(entry.thumbnail_4x, False),
			},
			'preview': {'1x': thumbnail_to_href(entry.preview_1x, True), '2x': thumbnail_to_href(entry.preview_2x, True)},
			'count': {'item': entry.count_item, 'link': entry.count_link, 'similar': entry.count_similar, 'duplicate': entry.count_duplicate},
		}
	)


def remove_none(input: dict[str, object]) -> dict[str, object]:
	result = {}
	for k, v in input.items():
		if isinstance(v, dict):
			v = remove_none(v) or None
		if v is not None:
			result[k] = v
	return result
