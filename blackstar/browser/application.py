import atexit
import importlib
import importlib.abc
import importlib.machinery
import importlib.resources
import os
import pathlib
import shutil
import tempfile

import bottle

from .authorization import AuthorizationPlugin
from .configuration import ConfigurationPlugin
from .database import DatabasePlugin
from .logging import ExceptionPlugin

ROOT_RESOURCE = importlib.resources.files(__package__)

ROOT_PATH: pathlib.Path
if isinstance(ROOT_RESOURCE, pathlib.Path):
	ROOT_PATH = ROOT_RESOURCE

elif isinstance(ROOT_RESOURCE, os.PathLike):
	ROOT_PATH = pathlib.Path(os.fspath(ROOT_RESOURCE))

else:
	ROOT_PATH = pathlib.Path(tempfile.mkdtemp(prefix='blackstar-'))
	atexit.register(shutil.rmtree, ROOT_PATH, ignore_errors=True)

	def _recurse(source: importlib.abc.Traversable, destination: pathlib.Path, skip: list[str]) -> None:
		for resource in source.iterdir():
			if resource.is_dir():
				_recurse(resource, destination / resource.name, skip)
				continue
			if not resource.is_file():
				continue
			if os.path.splitext(resource.name)[1].lower() in skip:
				continue
			destination.mkdir(parents=True, exist_ok=True)
			with resource.open('rb') as fp_in, (destination / resource.name).open('xb') as fp_out:
				shutil.copyfileobj(fp_in, fp_out)

	_recurse(ROOT_RESOURCE, ROOT_PATH, importlib.machinery.all_suffixes())
	del _recurse

CONTENT_PATH = ROOT_PATH / 'content'
TEMPLATE_PATH = ROOT_PATH / 'template'

bottle.TEMPLATE_PATH.clear()
bottle.TEMPLATE_PATH.append(os.fspath(TEMPLATE_PATH))

master = bottle.Bottle()
master.resources.base = os.fspath(ROOT_PATH)
master.uninstall('template')
master.uninstall('json')
master.install(ExceptionPlugin())
master.install(ConfigurationPlugin())
master.install(AuthorizationPlugin())
master.install(DatabasePlugin())

importlib.import_module('.static', __package__)
importlib.import_module('.error', __package__)
importlib.import_module('.login', __package__)
importlib.import_module('.thumbnail', __package__)
importlib.import_module('.dispatch', __package__)
importlib.import_module('.share', __package__)
importlib.import_module('.information.database', __package__)
importlib.import_module('.information.location', __package__)
importlib.import_module('.information.node', __package__)
