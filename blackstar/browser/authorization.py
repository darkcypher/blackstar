import enum
import time
import typing

import bottle

from .configuration import get_current as get_configuration
from .logging import LOGGER
from .token import VERSION, Token, TokenError

T = typing.TypeVar('T')
P = typing.ParamSpec('P')


class TokenSource(enum.Enum):
	UNKNOWN = enum.auto()
	QUERY = enum.auto()
	HEADER = enum.auto()
	COOKIE = enum.auto()
	DATABASE = enum.auto()


class GetUrlProtocol(typing.Protocol):
	def __call__(self, routename: str, **kwargs: object) -> str: ...


class AuthorizationPlugin:
	name = 'authorization'
	api = 2

	def __init__(self) -> None:
		self.app: typing.Optional[bottle.Bottle] = None
		self.get_url_real: typing.Optional[GetUrlProtocol] = None

	def setup(self, app: bottle.Bottle) -> None:
		self.app = app
		self.get_url_real = app.get_url
		app.get_url = self.get_url

	def close(self) -> None:
		assert self.app is not None
		self.app.get_url = self.get_url_real
		self.get_url_real = None
		self.app = None

	def apply(self, callback: typing.Callable[P, T], route: bottle.Route) -> typing.Callable[P, typing.Union[T, bottle.HTTPError]]:
		def wrapper(*args: P.args, **kwargs: P.kwargs) -> typing.Union[T, bottle.HTTPError]:
			bottle.request.time = int(time.time())
			token, source = get_token()
			anonymous: bool = route.config.get('anonymous', False)
			if token is None and not anonymous:
				return bottle.HTTPError(401, 'Invalid or Missing Token')
			update_environ(token)
			bottle.request.token = token
			bottle.request.token_source = source
			try:
				return callback(*args, **kwargs)
			finally:
				del bottle.request.token_source
				del bottle.request.token

		return wrapper

	def get_url(self, routename: str, **kwargs: object) -> str:
		assert self.get_url_real is not None

		if not kwargs.pop('pure', False) and getattr(bottle.request, 'token_source', TokenSource.UNKNOWN) == TokenSource.QUERY:
			token: typing.Optional[Token] = getattr(bottle.request, 'token', None)
			if token is not None:
				cfg = get_configuration()
				kwargs.setdefault(cfg.authentication.parameter, token.encode(cfg.authentication.secret))

		return self.get_url_real(routename, **kwargs)


def get_token() -> tuple[typing.Optional[Token], TokenSource]:
	token = get_token_query()
	if token is not None:
		return token, TokenSource.QUERY
	token = get_token_header()
	if token is not None:
		return token, TokenSource.HEADER
	token = get_token_cookie()
	if token is not None:
		return token, TokenSource.COOKIE
	return None, TokenSource.UNKNOWN


def get_token_query() -> typing.Optional[Token]:
	cfg = get_configuration()
	value = bottle.request.query.get(cfg.authentication.parameter)
	return decode_validate(value, TokenSource.QUERY)


def get_token_header() -> typing.Optional[Token]:
	header = bottle.request.headers.get('Authorization')
	if not isinstance(header, str):
		return None
	type, value = header.split(maxsplit=2)
	if type.lower() != 'token':
		return None
	return decode_validate(value, TokenSource.HEADER)


def get_token_cookie() -> typing.Optional[Token]:
	cfg = get_configuration()
	value = bottle.request.cookies.get(cfg.authentication.cookie)
	return decode_validate(value, TokenSource.COOKIE)


def set_token_cookie(token: typing.Optional[Token], sticky: bool) -> None:
	cfg = get_configuration()

	options: dict[str, object] = {'httponly': True, 'path': bottle.request.app.get_url('root', pure=True)}

	if bottle.request.environ.get('wsgi.url_scheme') == 'https':
		options['secure'] = True

	if token is None:
		bottle.response.delete_cookie(cfg.authentication.cookie, **options)
		return

	if sticky:
		options['expires'] = 0x7FFFFFFF if token.expiration is None else token.expiration

	bottle.response.set_cookie(cfg.authentication.cookie, token.encode(cfg.authentication.secret), **options)


def decode_validate(data: str, source: TokenSource) -> typing.Optional[Token]:
	if not data:
		return None

	cfg = get_configuration()

	try:
		token = Token.decode(data, cfg.authentication.secret)
	except TokenError as e:
		LOGGER.error('Failed to decode token from %s: %s', source.name.lower(), str(e))
		return None
	except Exception as e:
		LOGGER.error('Failed to decode token from %s:', source.name.lower(), exc_info=e)
		return None

	if token.version != VERSION:
		LOGGER.warning('Invalid token version (v%d != v%d)', token.version, VERSION)
		return None

	if token.is_expired_at(bottle.request.time):
		LOGGER.info('Token has expired (%d < %d)', token.expiration, bottle.request.time)
		return None

	if token.username is None:
		return token

	user = cfg.users.get(token.username)

	if user is None:
		LOGGER.info('Token user %s does not exist', token.username)
		return None

	if token.sequence is None:
		LOGGER.info('Missing token sequence number for %s', token.username)
		return None

	if token.sequence != user.sequence:
		LOGGER.info('Invalid token sequence number for %s (%d != %d)', token.username, token.sequence, user.sequence)
		return None

	return token


def update_environ(token: typing.Optional[Token]) -> None:
	if token is not None and token.username:
		bottle.request.environ['blackstar.username'] = token.username
	else:
		try:
			del bottle.request.environ['blackstar.username']
		except KeyError:
			pass
