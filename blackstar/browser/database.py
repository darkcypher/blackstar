import contextlib
import sqlite3
import typing

import bottle

from ..common.configuration import Configuration
from ..common.database import open_media_database, open_reference_database
from .configuration import get_current as get_configuration

T = typing.TypeVar('T')
P = typing.ParamSpec('P')


class DatabasePlugin:
	name = 'database'
	api = 2

	def apply(self, callback: typing.Callable[P, T], route: bottle.Route) -> typing.Callable[P, T]:
		media: typing.Optional[bool] = route.config.get('media', None)
		reference: typing.Optional[bool] = route.config.get('reference', None)

		if media is None and reference is None:
			return callback

		def wrapper(*args: P.args, **kwargs: P.kwargs) -> T:
			with self._open_db(open_media_database, media) as media_db:
				with self._open_db(open_reference_database, reference) as reference_db:
					try:
						bottle.local.media_db = media_db
						bottle.local.reference_db = reference_db
						return callback(*args, **kwargs)
					finally:
						del bottle.local.reference_db
						del bottle.local.media_db

		return wrapper

	def _open_db(
		self, opener: typing.Callable[[Configuration, bool], sqlite3.Connection], value: typing.Optional[bool]
	) -> contextlib.AbstractContextManager[typing.Optional[sqlite3.Connection]]:
		if value is None:
			return contextlib.nullcontext(None)
		else:
			return contextlib.closing(opener(get_configuration(), value))


def get_media_db() -> sqlite3.Connection:
	result: typing.Optional[sqlite3.Connection] = bottle.local.media_db
	if result is None:
		raise RuntimeError('media database not opened')
	return result


def get_reference_db() -> sqlite3.Connection:
	result: typing.Optional[sqlite3.Connection] = bottle.local.reference_db
	if result is None:
		raise RuntimeError('reference database not opened')
	return result
